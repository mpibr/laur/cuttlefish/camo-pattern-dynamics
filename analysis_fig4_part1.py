#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Figure 4. Organisation and reorganisation of chromatophore groupings during pattern transitions.
Part 1: the same examples as shown in:
Fig 4a-c
Fig 4f-h

@author: liangx
"""
##
import h5py
import os
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm
from texture import featurespace as fs
import joblib
from importlib import reload
import skimage.measure
from matplotlib import transforms
import scipy.ndimage
import sys
def prePCA(areas_sub):
    areas_sub_n = (areas_sub - areas_sub.mean(axis=0)) / areas_sub.std(axis=0)
    areas_sub_n[:, areas_sub.std(axis=0) == 0] = 0
    return areas_sub_n
home = os.environ['HOME']
os.chdir(home+'/Dropbox/analysis_liangx/')
indir = os.getcwd() + '/data_example/'
video_basename = '2020-11-23-14-53-15'

##load data, PCA, location of each chromatophore

if not os.path.isfile(indir + 'areas.PCA'):
    areas_f = h5py.File(indir + video_basename + '.areas', 'r')
    areas = areas_f['areas']
    frame_ind = np.array(areas_f['frame_indexes'])
    chunk_ind = np.array(areas_f['chunk_indexes'])
    cq_f = h5py.File(indir + video_basename + '.cleanqueen', 'r')
    cq = cq_f['Chromatophore']
    qf_f = h5py.File(indir + video_basename + '.queenframe', 'r')
    qf = qf_f['Chromatophore']
    masterframe = np.array(qf).astype(float) / 255
    mask = fs.cuttlefish_mask(masterframe, 300)
    masked = cq * mask[:cq.shape[0], :cq.shape[1]]
    mask_ind = np.unique(masked)[1:].astype('int')
    pca2 = PCA(n_components=200, svd_solver='full')
    areas_all = np.array(areas[:, mask_ind])
    SD = areas_all.std(axis=0)
    areas_all2 = (areas_all - areas_all.mean(axis=0)) / areas_all.std(axis=0)
    sd_select = np.where(SD > 0)[0]
    areas_all2 = areas_all2[:, sd_select]
    mask_ind2 = mask_ind[sd_select]
    skip = areas_all.shape[0] // 2000
    area_PCA = pca2.fit(areas_all2[::skip, :])
    points_all = area_PCA.transform(areas_all2)
    joblib.dump((points_all,mask_ind2), indir + 'areas.PCA')
else:
    points_all, mask_ind2 = joblib.load(indir + 'areas.PCA')
(areas_all,chunk_ind) = joblib.load( indir + '2020-11-23-14-53-15.areas')
cq_f = h5py.File(indir + video_basename + '.cleanqueen', 'r')
cq = cq_f['Chromatophore']
cq = np.array(cq).astype(int)
qf_f = h5py.File(indir + video_basename + '.queenframe', 'r')
qf = qf_f['Chromatophore']
masterframe = np.array(qf).astype(float) / 255
if not os.path.isfile(indir + 'draw_parameter2'):
    prop_rmask = skimage.measure.regionprops(cq)
    center = np.empty((2, len(prop_rmask)))
    i = 0
    for p in tqdm(prop_rmask):
        center[:, i] = np.round(p.centroid).astype(int)
        i += 1
    mask = fs.cuttlefish_mask(masterframe, 300)
    labels, num_labels = scipy.ndimage.label(mask)
    prop = fs.find_the_large_mask(labels.T, mask.T)
    joblib.dump((center, labels, mask), indir + 'draw_parameter2')
else:
    (center, labels, mask) = joblib.load(indir + 'draw_parameter2')
    prop = fs.find_the_large_mask(labels.T, mask.T)
crop = cq[prop._slice]
ratio = crop.shape[0] / crop.shape[1]
anlge = -prop.orientation / np.pi * 180
rot = transforms.Affine2D().rotate_deg(anlge - 90)
left, right, up, dwon = joblib.load(indir + 'mask_range')

##leiden clustering
import scanpy as sc

s1 = 13
resolution = 2

labels_chr_name = indir + str(s1) + '-only-chrom_cluster_label' + str(resolution)
if not os.path.isfile(labels_chr_name):
    points_sub, areas_sub, frame_sub, dur1 = fs.select_taj2([s1], areas_f, points_all, mask_ind2)
    areas_all = np.vstack((areas_sub))
    print(areas_all.shape)
    SD = areas_all.std(axis=0)
    areas_all2 = (areas_all - areas_all.mean(axis=0)) / areas_all.std(axis=0)
    areas_all2[:, np.where(SD == 0)[0]] = 0
    adata = ad.AnnData(areas_all2.T)
    sc.tl.pca(adata, svd_solver='arpack', n_comps=200)
    sc.pp.neighbors(adata, n_neighbors=n_neighbors, n_pcs=50)
    sc.tl.umap(adata, min_dist=0)
    sc.tl.leiden(adata, resolution=resolution)
    labels_chr = adata.obs['leiden'].to_numpy().astype('int')
    joblib.dump(labels_chr, labels_chr_name)
    print(len(np.unique(labels_chr)), 'compoments')
else:
    labels_chr = joblib.load(labels_chr_name)
    id_counts = np.array([[list(labels_chr).count(x)] for x in set(labels_chr)]).squeeze()


## Fig 4a-top
s1 = 13
mean_sub, std_sub, areas_sub, dur1 = fs.taj_decomp([s1], points_all, chunk_ind, areas_all, mask_ind2, labels_chr)
points_sub, speed, dur1 = fs.select_taj_speed([s1], points_all, chunk_ind, norm=False)
speed1 = fs.smooth(speed, 25)
trough = fs.find_trough(speed1, 35, 55, 1)
truncate = 1
if truncate:
    n1 = 2  # 13
    n2 = -3  # 13
    areas_sub = areas_sub[trough[n1]:trough[n2], :]
    points_sub = points_sub[trough[n1]:trough[n2], :]
    speed = speed[trough[n1]:trough[n2]]
    mean_sub = mean_sub[trough[n1]:trough[n2], :]
    std_sub = std_sub[trough[n1]:trough[n2], :]
    dur1 = [points_sub.shape[0]]
speed1 = fs.smooth(speed, 25)
trough2 = fs.find_trough(speed1, 55, 35, 1, 1)

plt.close('all')
s_win = 25
fig = plt.figure(figsize=(5, 4))
ax = fig.add_subplot(111)
n = 0
for i in range(len(dur1)):
    n1 = n + dur1[i]
    text_points = points_sub[n:n1, :]
    n = n1
    text_points2 = text_points.copy()
    text_points3 = text_points.copy()
    text_smooth = fs.NDsmooth(text_points2, s_win, 'flat')
    cm1 = plt.cm.get_cmap('hot')
    vs = fs.velocity(fs.d_F(text_points3, 1), 55)
    vs1 = vs.copy()
    vs2 = fs.smooth(vs1, 255, 'flat', 0)
    vs1 = vs1 - vs2 + vs2.mean()
    vs = fs.smooth(vs1, 25, 'flat')
    z1 = np.concatenate(([vs.mean()], vs), axis=0)
    ax.plot(text_smooth[:, 0], text_smooth[:, 1], color='k',linewidth=5,alpha=.5,zorder=1)

    sb1 = ax.scatter(text_smooth[:len(z1), 0], text_smooth[:len(z1), 1],
                    c=z1, s=5, vmin=z1.mean() - z1.std(), vmax=z1.mean() + z1.std() * 2,
                    cmap=cm1, alpha=.75,zorder=5)
    ax.plot(text_points[:, 0], text_points[:, 1],
            linewidth=.5, c='k', alpha=.5, label=i, zorder=0)
    ax.scatter(text_points[0, 0], text_points[0, 1],
               s=20, marker='o', c='k')
    ax.scatter(text_points[-1, 0], text_points[-1, 1],
               s=20, marker='o', c='k')
    ax.text(text_points[0, 0], text_points[0, 1],fontsize=24, s = 's', color='k', zorder=10)
    ax.text(text_points[-1, 0], text_points[-1, 1],fontsize=24, s = 'e', color='k',zorder=10)
    ax.scatter(text_points[trough2, 0], text_points[trough2, 1],
               s=1000, marker='o', c='b', alpha=0.2)
cbar = fig.colorbar(sb1, ax=ax, shrink=0.3, pad =0.05)
color_label = 'speed (a.u.)'
cbar.ax.set_ylabel(color_label,fontsize=20)
cbar.ax.tick_params(labelsize=18)
ax.set_xlabel('PC1',fontsize=18)
ax.set_ylabel('PC2',fontsize=18)
plt.tick_params(labelsize=12)
plt.locator_params(nbins=5)
plt.tight_layout()

#### find components

ratio_sd = (mean_sub.max(axis=0) - mean_sub.min(axis=0)) / (std_sub.mean(axis=0) / np.sqrt(id_counts))
id_s = ratio_sd[:26].argsort()[-12:]

#find fast phase
speed2 = speed.copy()
trough2 = fs.find_trough(speed2, 55, 75, 1,1)
trough = np.hstack((0, trough2, points_sub.shape[0]))
vs_areas = speed
rise = []
fall = []
peaks = []
for i in range(len(trough) - 1):
    peak = vs_areas[trough[i]:trough[i + 1]].argmax() + trough[i]
    peaks.append(peak)
    rising = vs_areas[trough[i]:peak]
    cut = np.quantile(rising, 0.15)
    rise.append(np.abs(rising - cut).argmin() + trough[i])
    falling = vs_areas[peak:trough[i + 1]]
    cut = np.quantile(falling, 0.25)
    fall.append(np.abs(falling - cut).argmin() + peak)
h = plt.hist(ratio_sd[:26], 50)
thresh = 18
id_s = np.where(ratio_sd > thresh)[0]
print(len(id_s))

peaks_list = []
for cluster_id in tqdm(id_s):
    areas_mean = mean_sub[:, cluster_id].copy()
    speed_c = fs.smooth(fs.d_F(areas_mean, 5), 55)
    thr = np.abs(speed_c - speed_c.mean()) > speed_c.std() * 2
    cross = np.nonzero(np.diff(thr))[0]
    if thr[0] > 0:
        cross = cross[1:]
    peaks = []
    for i in range(len(cross) // 2):
        peaks.append(np.abs(speed_c[cross[i * 2]:cross[i * 2 + 1]]).argmax() + cross[i * 2])
    peaks_list.append(peaks)
id_s2 = id_s
change = []
ind = []
for i in range(len(rise)):
    t1 = rise[i]
    t2 = fall[i]
    id = []
    for k in range(len(peaks_list)):
        peaks = peaks_list[k]
        if ((peaks < t2 + 25) * (peaks > t1 - 25)).any():
            id.append(id_s[k])
    if t1 < 0:
        t1 = 0
    if t2 >= mean_sub.shape[0]:
        t2 = mean_sub.shape[0] - 1
    fast_seg = mean_sub[t1:t2, id_s2].copy()
    fast_seg_std = std_sub[:, id_s2].mean(axis=0)
    sem = fast_seg_std / np.sqrt(id_counts[id_s2])
    Z_fast = (fast_seg.max(axis=0) - fast_seg.min(axis=0)) / sem
    change.append(Z_fast)
    thresh = Z_fast.mean() + Z_fast.std()*2.5
    id2 = id_s2[np.where(Z_fast > thresh)[0]]
    id = np.hstack((id, id2)).astype(int)
    print(np.unique(id))
    ind.append(np.unique(id))
change= np.array(change)
plt.figure()
h = plt.hist(change.flatten(),100,color='k')
thresh = 7.5
ind3 = [id_s2[np.where(change[x,:]>thresh)[0]] for x in range(change.shape[0])]
plt.close('all')


##Figure 4b - plot traces separately
from matplotlib import colors
select_id = np.unique(np.hstack((ind))).astype(int)
select_id1 = [17, 1, 7, 18] #13
cnames = ['red', 'blue', 'orange', 'green']
color2_1 = [colors.to_rgba(colors.cnames[x]) for x in cnames]
select_id0 = list(select_id)
[select_id0.remove(x) for x in select_id1]
color2_0 = plt.cm.jet(np.linspace(0, 1, len(select_id0) + 2))[2:]
color2 = np.vstack((color2_1, color2_0))
select_id = np.hstack((select_id1, select_id0))
joblib.dump((select_id1, select_id0, color2), indir + str(s1) + '_component_fast')

(select_id1, select_id0, color2) = joblib.load(indir + str(s1) + '_component_fast')
import matplotlib.gridspec as gridspec
fig = plt.figure(figsize=(9, 5))
spec2 = gridspec.GridSpec(ncols=1, nrows=4, figure=fig)
ax2 = fig.add_subplot(spec2[:2, :])
ax3 = fig.add_subplot(spec2[2:4, :])
time = np.arange(mean_sub.shape[0]) / 25
for cluster_id in tqdm(select_id1):
    areas_mean = mean_sub[:, cluster_id].copy()
    areas_std = std_sub[:, cluster_id]
    sem = areas_std / np.sqrt(id_counts[cluster_id]) / (areas_mean.max() - areas_mean.min())
    areas_mean = (areas_mean - areas_mean.min()) / (areas_mean.max() - areas_mean.min())
    color = color2[np.where(select_id == cluster_id)[0]][0]
    ax2.plot(time, areas_mean, color=color)
    ax2.fill_between(time, areas_mean - sem, areas_mean + sem,
                     color=color, alpha=0.3)

    # speed_c = fs.smooth(fs.d_F(areas_mean,5),55)
    # cross = speed_c < speed_c.mean() - speed_c.std() * 2
    # cross = np.nonzero(np.diff(cross))[0]
    # peaks = []
    # for i in range(len(cross)//2):
    #     peaks.append(speed_c[cross[i*2]:cross[i*2+1]].argmin() + cross[i*2])
    # ax2.vlines(peaks,ymin = 0, ymax = 1, color = color)

for cluster_id in tqdm(select_id0):
    areas_mean = mean_sub[:, cluster_id].copy()
    areas_std = std_sub[:, cluster_id]
    sem = areas_std / np.sqrt(id_counts[cluster_id]) / (areas_mean.max() - areas_mean.min())
    areas_mean = (areas_mean - areas_mean.min()) / (areas_mean.max() - areas_mean.min())
    color = color2[np.where(select_id == cluster_id)[0]][0]
    ax3.plot(time, areas_mean, color=color)
    ax3.fill_between(time, areas_mean - sem, areas_mean + sem,
                     color=color, alpha=0.3)
    # speed_c = fs.smooth(fs.d_F(areas_mean,5),55)
    # thr = np.abs(speed_c - speed_c.mean()) > speed_c.std() * 2
    # cross = np.nonzero(np.diff(thr))[0]
    # if thr[0]>0:
    #     cross = cross[1:]
    # peaks = []
    # for i in range(len(cross)//2):
    #     peaks.append(np.abs(speed_c[cross[i*2]:cross[i*2+1]]).argmax() + cross[i*2])
    # ax3.vlines(peaks,ymin = 0, ymax = 1, color = color)

ax3.set_ylabel('chrom. size',fontsize=18)
ax2.set_ylabel('chrom. size',fontsize=18)
ax2.locator_params(axis='y',nbins=4)
ax3.locator_params(axis='y',nbins=4)
ax3.locator_params(axis='x',nbins=5)
ax2.tick_params(labelsize=16)
ax3.tick_params(labelsize=16)

# ax1.plot(time, speed, c='k', linewidth=2)
for i in range(len(rise)):
    # ax1.fill_between(np.arange(rise[i] / 25, fall[i] / 25), speed.min(), speed.max(),
    #                  color='r', alpha=0.1)
    ax2.fill_between(np.arange(rise[i] / 25, fall[i] / 25), 0, 1, color='r', alpha=0.1)
    ax3.fill_between(np.arange(rise[i] / 25, fall[i] / 25), 0, 1, color='r', alpha=0.1)
# ax1.set_ylabel('speed')
# ax3.set_xticklabels([])
ax2.set_xticklabels([])
ax3.set_xlabel('time (s)',fontsize=18)
# plt.xlabel('time (s)',fontsize=18)
plt.tick_params(labelsize=16)
plt.tight_layout()

## Figure 4a-bottom: map increase vs decrease
plt.figure(figsize=(len(ind), 3))
for i in tqdm(range(len(ind))):
    ax1 = plt.subplot(1, len(ind), i + 1)
    base = ax1.transData
    ax1.imshow(255 - masterframe, transform=rot + base, cmap='gray', alpha=.5, origin="lower")
    for cluster_id in ind[i]:
        cc = center[:, mask_ind2[np.where(labels_chr == cluster_id)[0]] - 1]
        ax1.scatter(cc[1, :], cc[0, :], s=.1, alpha=.25,
                    transform=rot + base, color=color2[np.where(select_id == cluster_id)[0]][0])
    ax1.axis('off')
    ax1.set_xlim((left, right))
    ax1.set_ylim((up, dwon))
plt.tight_layout()

##fig 4c mixed heat map
select_id1_1 = [17, 1] # select two component examples for trial #13
if not os.path.isfile(indir + str(s1) + str(select_id1_1) + 'temp_f'):
    temp_f_list = []
    jump_list = []
    n = 1
    win = 50
    for cluster_id in select_id1_1:
        ax1 = plt.subplot(1, 4, n)
        n = n + 1
        chrom_ind = np.where(labels_chr == cluster_id)[0]
        areas_cluster = areas_sub[:, chrom_ind]
        areas_cluster_s = areas_cluster.copy()
        areas_cluster_s = fs.NDsmooth(prePCA(areas_cluster_s), 25)
        mean_trace = areas_cluster_s.mean(axis=1)
        average_jump = np.abs(mean_trace - mean_trace.mean())[:750].argmin()
        areas_cluster_s2 = areas_cluster_s[(average_jump - win):(average_jump + win), :].copy()
        areas_cluster_s2 = fs.NDsmooth(areas_cluster_s2, 55)
        temp_f = fs.sig_p(areas_cluster_s2) - win
        rank_cluster = temp_f.argsort()[::-1]
        temp_f_list.append(temp_f)
        jump_list.append(average_jump)
    joblib.dump((temp_f_list, jump_list), indir + str(s1) + str(select_id1_1) + 'temp_f')
else:
    temp_f_list, jump_list = joblib.load(indir + str(s1) + str(select_id1_1) + 'temp_f')
(select_id1, select_id0, color2) = joblib.load(indir + str(s1) + '_component_fast')
select_id = np.hstack((select_id1, select_id0))

cm1 = plt.cm.get_cmap('Reds')
cm2 = plt.cm.get_cmap('Blues')
cm = [cm1, cm2]
color2 = [ 'r','b']

import matplotlib.gridspec as gridspec
fig = plt.figure(figsize=(12, 8))
spec2 = gridspec.GridSpec(ncols=4, nrows=2, figure=fig)
ax1 = fig.add_subplot(spec2[:, :2])
select_id1_1 = [17, 1]
for i in range(2):
    cluster_id = select_id1_1[i]
    temp_f = temp_f_list[select_id1.index(cluster_id)]
    chrom_ind = np.where(labels_chr == cluster_id)[0]
    print(len(chrom_ind))
    areas_cluster = areas_sub[:, chrom_ind]
    rank_cluster = temp_f.argsort()[::-1]
    ax2 = fig.add_subplot(spec2[i, 2:])
    Y = prePCA(areas_cluster[:, rank_cluster]).T
    thresh = np.quantile(Y, 0.1)
    thresh2 = np.quantile(Y, 0.9)
    Y[Y<thresh] = np.nan
    ax2.imshow(Y, cmap=cm[i], vmin=0, vmax=thresh2, aspect='auto',
               extent=[0, areas_cluster.shape[0] / 25, 0, areas_cluster.shape[1]])
    ax2.locator_params(nbins=5)
    ax2.tick_params(labelsize=16)
    if i == 1:
        ax2.set_xlabel('time (s)', fontsize=18)
    if i == 0:
        ax2.set_xticklabels([])
    base = ax1.transData
    ax1.imshow(255 - masterframe, transform=rot + base, cmap='gray', alpha=.5, origin="lower")
    cc = center[:, mask_ind2[np.where(labels_chr == cluster_id)[0]] - 1]
    color = color2[i]
    sca1 = ax1.scatter(cc[1, :], cc[0, :], s=5, transform=rot + base, color=color,
                       marker='.', alpha=1)
    ax1.axis('off')
    ax1.set_xlim((left, right))
    ax1.set_ylim((up, dwon))
    plt.tight_layout()

##fig 4f

select1 = [19, 22]  # select two similar trials (mottled->disruptive)
points_sub,dur1 = fs.select_taj(select1,points_all,chunk_ind)
import seaborn as sns


link_color_pal = sns.color_palette("husl", len(dur1))
fig = plt.figure(figsize=(4, 4))
ax = fig.add_subplot(111)
n = 0
for i in range(len(dur1)):
    n1 = n + dur1[i]
    text_points = points_sub[n:n1, :]
    n = n1
    ax.plot(text_points[:, 0], text_points[:, 1],
            linewidth=2, c=link_color_pal[i], alpha=1, label=select1[i], zorder=0)
    ax.scatter(text_points[0, 0], text_points[0, 1],
               s=20, marker='o', c='k')
    ax.scatter(text_points[-1, 0], text_points[-1, 1],
               s=20, marker='o', c='k')
    ax.text(text_points[0, 0], text_points[0, 1],fontsize=24, s = 's', color='k', zorder=10)
    ax.text(text_points[-1, 0], text_points[-1, 1],fontsize=24, s = 'e', color='k',zorder=10)
ax.set_xlabel('PC1',fontsize=18)
ax.set_ylabel('PC2',fontsize=18)
plt.tick_params(labelsize=12)
plt.locator_params(nbins=5)
plt.tight_layout()

## fig 4g
# points_sub, areas_sub, frame_sub, dur1 = fs.select_taj2(select1, areas_all, points_all, mask_ind2)
mean_sub, std_sub, areas_sub, dur1 = fs.taj_decomp(select1, points_all, chunk_ind, areas_all, mask_ind2, labels_chr)

import seaborn as sns

link_color_pal = sns.color_palette("husl", len(dur1))
resolution = 2

s1 = select1[0]
labels_chr_name = indir +  str(s1) + '-only-chrom_cluster_label' + str(resolution)
labels_chr1 = joblib.load(labels_chr_name)
color1 = plt.cm.hsv(np.linspace(0, 1, len(np.unique(labels_chr1))))

s2 = select1[1]
labels_chr_name = indir +  str(s2) + '-only-chrom_cluster_label' + str(resolution)
labels_chr2 = joblib.load(labels_chr_name)
color2 = plt.cm.hsv(np.linspace(0, 1, len(np.unique(labels_chr2))))

##fig 4g1
cluster_id = 26
chrom_ind = np.where(labels_chr1 == cluster_id)[0]
clu_sub = labels_chr2[chrom_ind]

areas_cluster = areas_sub[:, chrom_ind]
rank_cluster = clu_sub.argsort()
reload(fs)
# re-rank
rerank = 1
if rerank:
    id_counts_sub = np.array([[list(clu_sub).count(x)] for x in set(clu_sub)]).squeeze()
    rank_id = np.unique(clu_sub)[id_counts_sub.argsort()[::-1]]
    mean_clu_sub = []
    for i in rank_id:
        mean_clu_sub.append(areas_cluster[dur1[0]:, np.where(clu_sub == i)[0]].mean(axis=1))
    mean_clu_sub = np.array(mean_clu_sub).T
    mean_clu_sub = prePCA(mean_clu_sub)
    corr_clu = []
    for i in range(len(rank_id)):
        corr_clu.append(np.corrcoef((mean_clu_sub[:, i], mean_clu_sub[:, 0]))[1, 0])
    rank_corr = rank_id[np.array(corr_clu).argsort()[::-1]]
    rank_cluster = []
    for i in rank_corr:
        rank_cluster = np.hstack((rank_cluster, np.where(clu_sub == i)[0]))
    rank_cluster = rank_cluster.astype(int)
#

plt.figure(figsize=(12, 5))
ax1 = plt.subplot(1, 4, 1)
fs.plot_chrom_map(chrom_ind, ax1, masterframe, labels_chr1, mask_ind2, indir)
ax2 = plt.subplot(1, 4, 2)

areas_clusters = prePCA(areas_cluster[:dur1[0], rank_cluster][:,:])
fs.plt_chrom_heat(areas_clusters, rank_cluster, ax2, chrom_ind, labels_chr1)
ax2.hlines(areas_clusters.shape[1] + 10, xmin=0, xmax=areas_clusters.shape[0] / 25 + 5, linewidth=10,
           color=link_color_pal[0])
plt.locator_params(nbins=4)
plt.tick_params(labelsize=16)

ax3 = plt.subplot(1, 4, 3)
fs.plot_chrom_map(chrom_ind, ax3, masterframe, labels_chr2, mask_ind2, indir)
ax4 = plt.subplot(1, 4, 4)

areas_clusters = prePCA(areas_cluster[dur1[0]:, rank_cluster][:,:])
fs.plt_chrom_heat(areas_clusters, rank_cluster, ax4, chrom_ind, labels_chr2)
ax4.hlines(areas_clusters.shape[1] + 10, xmin=0, xmax=areas_clusters.shape[0] / 25 + 5, linewidth=10,
           color=link_color_pal[1])
plt.locator_params(nbins=4)
plt.tick_params(labelsize=16)

##fig 4g2
cluster_id = 17
chrom_ind = np.where(labels_chr2 == cluster_id)[0]
clu_sub = labels_chr1[chrom_ind]


areas_cluster = areas_sub[:, chrom_ind]
rank_cluster = clu_sub.argsort()
reload(fs)
# re-rank
rerank = 1
if rerank:
    id_counts_sub = np.array([[list(clu_sub).count(x)] for x in set(clu_sub)]).squeeze()
    rank_id = np.unique(clu_sub)[id_counts_sub.argsort()[::-1]]
    mean_clu_sub = []
    for i in rank_id:
        mean_clu_sub.append(areas_cluster[dur1[0]:, np.where(clu_sub == i)[0]].mean(axis=1))
    mean_clu_sub = np.array(mean_clu_sub).T
    mean_clu_sub = prePCA(mean_clu_sub)
    corr_clu = []
    for i in range(len(rank_id)):
        corr_clu.append(np.corrcoef((mean_clu_sub[:, i], mean_clu_sub[:, 0]))[1, 0])
    rank_corr = rank_id[np.array(corr_clu).argsort()[::-1]]
    rank_cluster = []
    for i in rank_corr:
        rank_cluster = np.hstack((rank_cluster, np.where(clu_sub == i)[0]))
    rank_cluster = rank_cluster.astype(int)
#

plt.figure(figsize=(12, 5))
ax1 = plt.subplot(1, 4, 1)
fs.plot_chrom_map(chrom_ind, ax1, masterframe, labels_chr1, mask_ind2, indir)
ax2 = plt.subplot(1, 4, 2)

areas_clusters = prePCA(areas_cluster[:dur1[0], rank_cluster][:,:])
fs.plt_chrom_heat(areas_clusters, rank_cluster, ax2, chrom_ind, labels_chr1)
ax2.hlines(areas_clusters.shape[1] + 10, xmin=0, xmax=areas_clusters.shape[0] / 25 + 5, linewidth=10,
           color=link_color_pal[0])
plt.locator_params(nbins=4)
plt.tick_params(labelsize=16)

ax3 = plt.subplot(1, 4, 3)
fs.plot_chrom_map(chrom_ind, ax3, masterframe, labels_chr2, mask_ind2, indir)
ax4 = plt.subplot(1, 4, 4)

areas_clusters = prePCA(areas_cluster[dur1[0]:, rank_cluster][:,:])
fs.plt_chrom_heat(areas_clusters, rank_cluster, ax4, chrom_ind, labels_chr2)
ax4.hlines(areas_clusters.shape[1] + 10, xmin=0, xmax=areas_clusters.shape[0] / 25 + 5, linewidth=10,
           color=link_color_pal[1])
plt.locator_params(nbins=4)
plt.tick_params(labelsize=16)

###fig 4h Sankey plot shows re-organization of components

import plotly.graph_objects as go
import plotly.io as pio
pio.renderers.default = "browser"

select1 = [19,22]  # mottled->disruptive
resolution=2
s1 = select1[0]
labels_chr_name = indir +str(s1) + '-only-chrom_cluster_label' + str(resolution)
labels_chr1 = joblib.load(labels_chr_name)
color1 = plt.cm.hsv(np.linspace(0, 1, len(np.unique(labels_chr1))))
n1 = len(np.unique(labels_chr1))
id_counts1 = np.array([[list(labels_chr1).count(x)] for x in set(labels_chr1)]).squeeze()
mean_sub, std_sub, areas_sub, dur1 = fs.taj_decomp(select1, points_all, chunk_ind, areas_all, mask_ind2, labels_chr1)
ratio_sd = (mean_sub.max(axis=0) - mean_sub.min(axis=0)) / (std_sub.mean(axis=0) / np.sqrt(id_counts1))
id_s1 = ratio_sd.argsort()[-12:]

s2 = select1[1]
labels_chr_name = indir + str(s2) + '-only-chrom_cluster_label' + str(resolution)
labels_chr2 = joblib.load(labels_chr_name)
color2 = plt.cm.hsv(np.linspace(0, 1, len(np.unique(labels_chr2))))
n2 = len(np.unique(labels_chr2))
id_counts2 = np.array([[list(labels_chr2).count(x)] for x in set(labels_chr2)]).squeeze()
mean_sub, std_sub, areas_sub, dur1 = fs.taj_decomp(select1, points_all, chunk_ind, areas_all, mask_ind2, labels_chr2)
ratio_sd = (mean_sub.max(axis=0) - mean_sub.min(axis=0)) / (std_sub.mean(axis=0) / np.sqrt(id_counts2))
id_s2 = ratio_sd.argsort()[-12:]

dis_mat = np.zeros((n1,n2))
source = []
target = []
value = []
clink = []

for i in tqdm(id_s1):
    for j in id_s2:
        chrom_ind1 = np.where(labels_chr1 == i)[0]
        chrom_ind2 = np.where(labels_chr2 == j)[0]
        share_n = len(set(chrom_ind1).intersection(chrom_ind2))
        ratio = share_n*2/(len(chrom_ind1) + len(chrom_ind2))
        ratio1 = share_n/len(chrom_ind1)
        ratio2 = share_n / len(chrom_ind2)
        source.append(i)
        target.append(j+n1)
        value.append(share_n)
        c = (color1[i] * 255)
        if ratio > .05:
            clink.append("rgba(0, 0, 0, .5)")
        else:
            clink.append("rgba(50, 50, 50, .1)")

cnode = []
for i in color1:
    i =(i*255)
    cnode.append('rgb'+str((i[0],i[1],i[2])))
for i in color2:
    i =(i*255)
    cnode.append('rgb'+str((i[0],i[1],i[2])))

link = dict(source = source, target = target, value = value, color = clink)
label = list(map(str, np.hstack((np.arange(0,n1),np.arange(0,n2)))))

node = dict(label = label, pad=5, thickness=20,color = cnode)
data = go.Sankey(textfont=dict(color="rgba(0,0,0,0)", size=1),link = link, node=node)

print(data)
layout = go.Layout(
    paper_bgcolor='rgba(0,0,0,0)',
    plot_bgcolor='rgba(0,0,0,0)'
)

fig = go.Figure(data=data,layout = layout)
fig.show()

##
