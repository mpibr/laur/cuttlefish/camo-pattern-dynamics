#!/usr/bin/env python
# coding: utf-8
"""
Figure 1cd. Camouflage-pattern space. One animal of ten analysed.
@author: woot
"""

from glob import glob
import os
import h5py
from importlib import reload
import pickle
import datetime

import detectron2
import umap

import numpy as np
import matplotlib.pyplot as plt
import cv2
import matplotlib.patches as patches
from matplotlib.offsetbox import AnchoredText
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
plt.rcParams['svg.fonttype'] = 'none'

import tensorflow as tf

from sklearn.decomposition import PCA
from scipy.stats import zscore

from tqdm import tqdm

from geosketch import gs

from texture import util
import analysis_util

## Define paths and variables

INDIR = 'data_example/input_dir'
OUTDIR = 'data_example/output_dir'
ANIMAL = 'sepia218'
ACTIVATION_SUFFIX = '.activ'
SAMPLING_RATE = 10
MIN_MASK_FRACTION = 0.04

CUTTLEFISH_MODEL = 'data_example/models/cuttlefish-seg_20200605.pth'
CUTTLEFISH_MODEL_BASE = 'COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml'
ROTATE_MODEL = 'data_example/models/cuttlefish-kp_20210820b.pth'
ROTATE_MODEL_BASE = 'COCO-Keypoints/keypoint_rcnn_R_50_FPN_3x.yaml'
MODEL_TYPE = 'detectron2'

MANTLE_LENGTH_MM = 57
MANTLE_LENGTH_SEG_PX = 3000
MANTLE_LENGTH_OVERVIEW_PX = 400
overview_scale = MANTLE_LENGTH_MM / MANTLE_LENGTH_OVERVIEW_PX
seg_scale = MANTLE_LENGTH_MM / MANTLE_LENGTH_SEG_PX

AREA_OUTLIERS = 2

RANDOM_SEED = 99
FONT_SIZE = 18

CODE_DIR = os.path.join(os.getcwd(), 'texture')
FIG_DIR = 'figures/'

###########################################################

## Load models

cuttlefish_model = util.load_detectron2_model(CUTTLEFISH_MODEL, CUTTLEFISH_MODEL_BASE)
upright_model = util.load_detectron2_model(ROTATE_MODEL, ROTATE_MODEL_BASE)

## Load all low-res videos from one animal

sessions = np.sort([os.path.split(f)[1] \
    for f in glob(os.path.join(OUTDIR, 'ex*', f'{ANIMAL}*'))])
vid_files = np.array([glob(os.path.join(INDIR, f'{s}/rec2_cam0_*.avi')) for s in sessions])

print('num_vid:', len(vid_files))

## Preprocessing

### Get activations
### Copy command and submit to SLURM cluster.

JOB_NAME = 'get_activation'
SLURM_ARG = ''  # e.g. partition=

for v in vid_files:
    sess = os.path.split(os.path.dirname(v))[1]
    prefix = '-'.join(sess.split('-')[1:])
    outfile = os.path.join(OUTDIR, sess, prefix) + ACTIVATION_SUFFIX

    print(f'mkdir -p {os.path.dirname(outfile)}; \
sbatch -n 1 --cpus-per-task 32 --mem 0 {SLURM_ARG} --job-name={JOB_NAME} \
-o {outfile}.log \
--wrap="srun --mpi=openmpi {os.path.join(CODE_DIR, "get_activations.py")} \
--video {v} {outfile} \
--cuttlefish-model {CUTTLEFISH_MODEL} \
--rotate-model {ROTATE_MODEL} \
--cuttlefish-model-dt2-base {CUTTLEFISH_MODEL_BASE} \
--rotate-model-dt2-base {ROTATE_MODEL_BASE} \
--cuttlefish-model-type {MODEL_TYPE} --rotate-model-type {MODEL_TYPE} \
--sampling-rate={SAMPLING_RATE} --eq-hist=True \
-max-batch-size=150 --min-mask-fraction={MIN_MASK_FRACTION} --rerun"')


###########################################################

## Load activation files

ACT_FILES = np.sort(glob(os.path.join(OUTDIR, '{}-*'.format(ANIMAL), '*{}'.format(ACTIVATION_SUFFIX))))
print('Number of act files:', np.sum([os.path.isfile(f) for f in ACT_FILES]))

activations, vid_idx, frame_numbers, areas, positions, vid_dict, radius_dict = analysis_util.load_activations(ACT_FILES)
session_list, type_list = analysis_util.get_session_type(vid_dict, OUTDIR)
print(activations.shape)


## Filter out erroneously segmented frames

AREA_THRESHOLD_HIGH = areas.mean() + areas.std()*AREA_OUTLIERS
AREA_THRESHOLD_LOW = areas.mean() - areas.std()*AREA_OUTLIERS
TYPE_FILTER = np.array([('ex2' in t) | ('ex3' in t) | ('ex4' in t) for t in type_list])[vid_idx]

data_filter = (areas >= AREA_THRESHOLD_LOW) & (areas <= AREA_THRESHOLD_HIGH) & TYPE_FILTER

activations_f = activations[data_filter]
vid_idx_f = vid_idx[data_filter]
frame_numbers_f = frame_numbers[data_filter]
positions_f = positions[data_filter]
areas_list_f = areas[data_filter]

print(AREA_OUTLIERS, activations_f.shape)

fig, ax = plt.subplots()
ax.hist(areas, bins=50, color='k', density=True, histtype='step')
ax.hist(areas_list_f, bins=50, color='r', density=True, histtype='step')
plt.show()

## Use UMAP as a guide to remove bad points from training (will keep in final plot)

### Fit UMAP

DATA = activations_f
np.random.seed(RANDOM_SEED)
DATA_SUBSAMPLED = DATA[np.random.choice(np.arange(len(DATA)), size=5000, replace=False)]
N_NEIGHBOURS = 100

print('umap_input:', DATA_SUBSAMPLED.shape)

u_model = umap.UMAP(n_neighbors=N_NEIGHBOURS, random_state=RANDOM_SEED, 
                    verbose=True).fit(DATA_SUBSAMPLED)
print('fitting done')
u = u_model.transform(DATA)

print('umap_output:', u.shape)

### Plot UMAP

U_EMBED = u
SESS_DICT, SESS_ID = np.unique(session_list[vid_idx_f.reshape(-1)], return_inverse=True)

fig, ax = plt.subplots(1, 2, figsize=(10,5))

ax[0].scatter(U_EMBED[:,0], U_EMBED[:,1], c='k', s=0.01)

ax[1].scatter(U_EMBED[:,0], U_EMBED[:,1], c=SESS_ID, cmap='viridis', s=0.01)

plt.show()

### PCA after filtering

DATA = activations_f
DATA = zscore(DATA, axis=0)
U_EMBED = u

print('pca_input:', DATA.shape)
DATA_SUBSAMPLED = DATA[U_EMBED[:,0]<5]  # threshold set based on UMAP visualisation
print('pca_input:', DATA_SUBSAMPLED.shape)

PCA_THRESHOLD = 0.8

pca = PCA(n_components=300, random_state=RANDOM_SEED)
pca.fit(DATA_SUBSAMPLED)  # n_samples, n_features

X_pca = pca.transform(DATA)
cumsum = np.cumsum(pca.explained_variance_ratio_)
n_components = (cumsum > PCA_THRESHOLD).nonzero()[0][0]
print(n_components)

### Variance explained

fig, ax = plt.subplots()
analysis_util.remove_padding()
ax.plot(cumsum, c='k')
ax.axvline(n_components, c='b')
ax.set_ylabel('Variance explained')
ax.set_xlabel('PCs')
plt.show()

### First 50 PCs

fig, ax = plt.subplots()
analysis_util.remove_padding()
ax.bar(np.arange(len(pca.explained_variance_ratio_))[:50], pca.explained_variance_ratio_[:50], color='k')
ax.set_ylabel('Variance explained')
ax.set_xlabel('PC')
plt.show()

## Get representitive sample of datapoints using geosketch

N_COMPONENTS = n_components
N_SAMPLES = 20000
U_EMBED = u
DATA = X_pca[U_EMBED[:,0]<5,:N_COMPONENTS]

print('n_components:', N_COMPONENTS)

sketch_index = gs(DATA, N_SAMPLES, replace=False, seed=RANDOM_SEED, verbose=True)
sketch_index = np.arange(len(X_pca))[U_EMBED[:,0]<5][sketch_index]

## Re-fit UMAP

DATA = activations_f
DATA_SUBSAMPLED = DATA[sketch_index]
N_NEIGHBOURS = 100

print('umap_input:', DATA_SUBSAMPLED.shape)

u_model_new = umap.UMAP(n_neighbors=N_NEIGHBOURS, random_state=RANDOM_SEED, 
                        min_dist=0.8, verbose=True).fit(DATA_SUBSAMPLED) 
print('fitting done')

u_new = u_model_new.transform(DATA)
print('umap_output:', u.shape)

### Save UMAP output and embedding

OUTPUT_FILE = os.path.join(OUTDIR, 'fig1/b.umap')
pickle.dump(u_model_new, open(OUTPUT_FILE, 'wb'))

EMBED_FILE = os.path.join(OUTDIR, 'fig1/b.embedding')
with h5py.File(EMBED_FILE, 'r') as hf:
    hf.create_dataset('embedding', data=u_new, dtype='float32')
    hf.create_dataset('frames_numbers', data=frame_numbers_f, dtype='int32')
    hf.create_dataset('video_indices', data=vid_idx_f, dtype='uint8')
    hf.create_dataset('vid_list', data=vid_dict.astype(h5py.string_dtype(encoding='utf-8')))
    hf.create_dataset('r_list', data=radius_dict, dtype='int32')
    hf.create_dataset('act_files', data=ACT_FILES.astype(h5py.string_dtype(encoding='utf-8')))
    hf.create_dataset('positions', data=positions_f, dtype='float32')
    hf.create_dataset('sketch_index', data=sketch_index, dtype='int32')
    hf.create_dataset('X_pca', data=X_pca, dtype='float32')


###########################################################

## Load embed file

with h5py.File(EMBED_FILE, 'r') as hf:
    u_new = hf['embedding'][:]
    frame_numbers_f = hf['frames_numbers'][:]
    vid_idx_f = hf['video_indices'][:]
    vid_dict = hf['vid_list'][:]
    radius_dict = hf['r_list'][:]
    positions_f = hf['positions'][:]
    act_files = hf['act_files'][:]
    sketch_index = hf['sketch_index'][:]
    X_pca = hf['X_pca'][:]

session_list, type_list = analysis_util.get_session_type(vid_dict, OUTDIR)

### Calculate size of dataset

total_frames = np.sum([frame_numbers_f[vid_idx_f==i].max() - frame_numbers_f[vid_idx_f==i].min() \
    for i in np.unique(vid_idx_f)])
dates = np.sort(np.unique([s.split('-')[1] for s in sessions[np.unique(vid_idx_f)]]))

print('total hours', total_frames/25/60/60)
print('total frames', len(np.unique(vid_idx_f)))

## Panel c: scatter with boxes indicating zoomed-in regions

ZOOM_IN = np.array([[9, 11.5], [7, 9.5], [13,9], [1.75,6.5],
                    [9,5.75], [3.25, 3], [8,3.25], [13,3.5]])
FLIP_IDX = [None, None, None, [(0,2)], [(1,1), (2,2)], [(2,1)], None, None]
U_EMBED = u_new
ZOOM_IN_SIZE = 0.8
N_COLS = 4
VIDEOS = vid_dict[vid_idx_f]
FRAME_NUMBERS = frame_numbers_f
RADIUS = (radius_dict[vid_idx_f]//1.8).astype(int)

N_GRID = (3,3)
GRID_SIZE = (64,64)
NEIGHBOUR_DISTANCE_CUTOFF = 0.1

n_rows = int(np.ceil(len(ZOOM_IN)/N_COLS))

fig, ax = plt.subplots(1,figsize=(6,6))
ax.scatter(U_EMBED[:,0], U_EMBED[:,1], c='k', s=0.05, alpha=0.2, rasterized=True)
analysis_util.remove_lines()
for j, (x, y) in enumerate(ZOOM_IN):
    for i in range(2):
        if i == 1:
            xx, yy = np.meshgrid(np.linspace(x, x+ZOOM_IN_SIZE, N_GRID[1]), 
                                 np.linspace(y, y+ZOOM_IN_SIZE, N_GRID[0]))
            rect = patches.Rectangle((x,y), ZOOM_IN_SIZE, ZOOM_IN_SIZE, 
                                     linewidth=2, edgecolor='none', facecolor='w',alpha=0.8)
            ax.add_patch(rect)
            ax.text(x+ZOOM_IN_SIZE/2, y+ZOOM_IN_SIZE/2,str(j+1), 
                    size=FONT_SIZE,color='k', ha='center', va='center')
plt.show()

today_str = datetime.datetime.now().strftime('%Y%m%d')
OUTPUT_FIG = os.path.join(FIG_DIR, f'fig1/e_scatter-regions_{today_str}.svg')
fig.savefig(OUTPUT_FIG, dpi=300, bbox_inches='tight', transparent=True)

## Panel d: zoomed-in regions

for i, (x,y) in enumerate(ZOOM_IN):
    fig, ax = plt.subplots(figsize=(6,6))
    cuttlefish_grid, mat_extent, mat_aspect = util.make_pattern_map(\
        U_EMBED, VIDEOS, FRAME_NUMBERS, RADIUS,
        cuttlefish_model, None, MODEL_TYPE,
        max_dist=NEIGHBOUR_DISTANCE_CUTOFF, n_grid=N_GRID, grid_size=GRID_SIZE, 
        zoom_in=(x,y,ZOOM_IN_SIZE,ZOOM_IN_SIZE))
    if FLIP_IDX[i] is not None:
        cuttlefish_grid = analysis_util.manual_flip_pattern_map(cuttlefish_grid, FLIP_IDX[i], N_GRID)
    ax.matshow(cuttlefish_grid, extent=mat_extent, origin='lower', aspect=mat_aspect)
    at = AnchoredText(
        '{}'.format(i+1),
        prop=dict(size=FONT_SIZE*2,color='k', backgroundcolor='w'), 
        frameon=False, loc='upper left',pad=0,borderpad=0)
    ax.add_artist(at)
    analysis_util.remove_lines()
    plt.show()

    today_str = datetime.datetime.now().strftime('%Y%m%d')
    OUTPUT_FIG = os.path.join(FIG_DIR, f'fig1/e_scatter-zooms-{i+1}_{today_str}.svg')
    fig.savefig(OUTPUT_FIG, dpi=300, bbox_inches='tight', transparent=True)