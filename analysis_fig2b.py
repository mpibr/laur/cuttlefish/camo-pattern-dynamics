#!/usr/bin/env python
# coding: utf-8
"""
Figure 2b. The relationship between camouflage and chequerboard backgrounds in skin-pattern space. 
Three animals plotted.
@author: woot
"""

import os
import datetime

# These two need to come in front in lab3 for some reason
import detectron2

import numpy as np
import cv2
import matplotlib.pyplot as plt
from tqdm import tqdm
import h5py

import seaborn as sns
sns.set_theme(style='ticks', palette='tab10')
import pandas as pd

from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

from sklearn.metrics import r2_score

from statsmodels.stats.multitest import multipletests

import statsmodels.api as sm

plt.rcParams['svg.fonttype'] = 'none'

from texture import util

## Define paths and variables

OVERVIEW_VID_DIRS = ['data_example/input_dir']
OUTDIR = 'data_example/output_dir'

CUTTLEFISH_MODEL = 'data_example/models/cuttlefish-seg_20200605.pth'
CUTTLEFISH_MODEL_BASE = 'COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml'
ROTATE_MODEL = 'data_example/models/cuttlefish-kp_20210820b.pth'
ROTATE_MODEL_BASE = 'COCO-Keypoints/keypoint_rcnn_R_50_FPN_3x.yaml'
MODEL_TYPE = 'detectron2'
LAYER = 'block5_conv1'
RANDOM_SEED = 99

FONT_SIZE = 18
FIELD_OF_VIEW_CM = 0.3906*13
FIELD_OF_VIEW_PX = 3000

FIG_DIR = 'figures/'

###########################################################

## Load model

cuttlefish_model = util.load_detectron2_model(CUTTLEFISH_MODEL, CUTTLEFISH_MODEL_BASE)
upright_model = util.load_detectron2_model(ROTATE_MODEL, ROTATE_MODEL_BASE)

## Load data

ACT_FILES = np.array([
    os.path.join(OUTDIR, 'sepia209_checkerboard_5.activations'),
    os.path.join(OUTDIR, 'sepia211_checkerboard_5_20220906.activations'),
    os.path.join(OUTDIR, 'sepia223_checkerboard_5.activations')])

animals = ['sepia209', 'sepia211', 'sepia223']
check_set = np.concatenate([np.repeat('coarse', 10),
                            np.repeat('fine', 19)])


## Fit PCA with all data

N_COMPONENTS = 50

data_train = []

for i, actfile in enumerate(ACT_FILES):
    with h5py.File(actfile, 'r') as hf:
        activations = hf['activations'][:]
    data_train.append(activations)
    
DATA_TRAIN = np.concatenate(data_train)

scaler = StandardScaler()
DATA_TRAIN = scaler.fit_transform(DATA_TRAIN)
pca = PCA(n_components=N_COMPONENTS, random_state=RANDOM_SEED)
pca.fit(DATA_TRAIN)  # n_samples, n_features

### Build pca dataframe

pca_df = pd.DataFrame()

for i, actfile in enumerate(ACT_FILES):
    with h5py.File(actfile, 'r') as hf:
        activations = hf['activations'][:]
    
        vid_dict = hf.attrs['vid_dict']
        radius_dict = hf.attrs['radius_dict']
        pp_vid = hf['pp_vid'][:]
        pp_frame = hf['pp_frame'][:]
        pp_trial = hf['pp_trial'][:]
        pp_stim = hf['pp_stim'][:]
        square_standard_cm = hf['square_standard_cm'][:]

    activations_mean = np.array([activations[pp_trial==t].mean(axis=0) for t in np.unique(pp_trial)])
    pp_vid_mean = np.array([pp_vid[pp_trial==t][0] for t in np.unique(pp_trial)])
    pp_stim_mean = np.array([pp_stim[pp_trial==t][0] for t in np.unique(pp_trial)])
    pp_frame_mean = np.array([pp_frame[pp_trial==t][len(pp_frame[pp_trial==t])//2] for t in np.unique(pp_trial)])

    ### Transform mean frame per trial

    DATA = activations_mean
    STIM = pp_stim_mean

    DATA = scaler.transform(DATA)
    DATA = pca.transform(DATA)

    ### build single dataset dataframe

    df = pd.DataFrame(DATA)
    df.columns = [f"PC{i}" for i in np.arange(N_COMPONENTS)+1]

    df['animal'] = animals[i]
    df['stim_idx'] = STIM
    df['stim_set'] = check_set[STIM]
    df['stim'] = square_standard_cm[STIM]
    df['video'] = vid_dict[pp_vid_mean]
    df['frame'] = pp_frame_mean
    df['radius'] = radius_dict[pp_vid_mean]

    pca_df = pd.concat([pca_df, df])

pca_df.animal = pca_df.animal.astype("category")
pca_df.video = pca_df.video.astype("category")
pca_df.stim_set = pca_df.stim_set.astype("category")
pca_df['animal_stim'] = [f"{a}_{b}" for a, b in zip(pca_df.animal.values, pca_df.stim_set.values)]
pca_df = pca_df.reset_index(drop=True)

###########################################################

## Panel b: PC1 vs checkerboard square size

pd.options.mode.chained_assignment = None
df = pca_df
df = df[df.animal.isin(['sepia209', 'sepia211', 'sepia223'])]
df['animal'] = df.animal.cat.remove_unused_categories()

fig, ax = plt.subplots(figsize=(7,6))

sns.lineplot(data=df, hue='animal',
        x="stim", y="PC1", err_style='bars',
        marker='o', ax=ax, lw=3, palette='Set2', legend=False)
ax.set_xscale('log')
ax.set_yticks([])

row_labels = square_standard_cm[:10]
ax.set_xticks(row_labels)
row_labels = ['{:.02f}'.format(l) for l in row_labels]
ax.set_xticklabels(row_labels, fontsize=FONT_SIZE)
ax.tick_params(axis='both', labelsize=FONT_SIZE)
ax.set_ylabel('Skin texture PC1', fontsize=FONT_SIZE)
ax.set_xlabel('Square size (cm)', fontsize=FONT_SIZE)
ax.minorticks_off()

ylim = ax.get_ylim()
ax.set_xlim(0.07,10.5)

ax.axvspan(0.3125, 1.25, color='k', alpha=0.1)
plt.show()

today_str = datetime.datetime.now().strftime('%Y%m%d')
OUTPUT_FIG = os.path.join(FIG_DIR, f'fig2/b_pc1_{today_str}.svg')
fig.savefig(OUTPUT_FIG, dpi=300, bbox_inches='tight', transparent=True)


## Regression

df = pca_df
df = df[df.animal.isin(['sepia209', 'sepia211', 'sepia223'])]
df = df[((df.stim>=0.3125) & (df.stim<=1.25))]
df['animal'] = df.animal.cat.remove_unused_categories()

reg_df = pd.DataFrame()

for i in range(1,N_COMPONENTS+1):
    for animal in np.unique(df.animal):
        df_l = df[(df.stim_set=='fine') & (df.animal==animal)]

        X = np.log2(df_l['stim'])
        y = df_l[f'PC{i}']
        X2  = sm.add_constant(X)
        mod  = sm.OLS(y, X2)
        res  = mod.fit()

        reg_df = reg_df.append({
            'r_squared': res.rsquared,
            'p_value': res.pvalues.stim,
            'PC': i,
            'animal': animal
        }, ignore_index=True)
        
_, reg_df['p_value_corrected'], _, _ = multipletests(reg_df.p_value, method='holm-sidak')