import os
import subprocess
import joblib
from tqdm import tqdm
from texture import featurespace as fs
from importlib import reload
from sklearn.decomposition import PCA
reload(fs)
import cv2
import matplotlib.pyplot as plt
import numpy as np


home = os.environ['HOME']
os.chdir(home+'/Dropbox/PycharmProjects/cep_stim/')
code_dir = home+'/Dropbox/PycharmProjects/cep_stim/'



exp_list=['sepia217-20210105-003']
azim = -50
elev = -105

for exp in exp_list:
    input='/gpfs/laur/sepia_tools/'+ exp + '/'
    # input='/gpfs/laur/sepia_compressed/'+ exp + '/'

    # input =  '/media/liangx/Data/'+ exp + '/'
    # input='/gpfs/laur/sepia_tools/'+ exp + '/'
    output  = home+'/Dropbox/lab2/sepia/' + exp + '/'
    output1 = '/media/liangx/Data/'+ exp + '/'
    try:
        os.mkdir(output1)
        os.mkdir(output)
    except:
        pass

    outname = home + '/Dropbox/lab2/sepia/' + exp + '/' + exp
    chunk=joblib.load(outname + '.textchunk')
    #################################chunking

    chunk = chunk[:,::-1]
    for ck in tqdm(range(chunk.shape[1])):
        n_point = chunk[0, ck] - chunk[1, ck]
        print(chunk[0, ck], chunk[1, ck])
        if not os.path.isfile(output+'overview' +str(chunk[0, ck])+'-'+str(chunk[1, ck])+ '.mp4'):
            job_args = ['python3',os.path.join(code_dir, 'texture_detectron_movie4.py'),input,
                        '--start',str(chunk[0, ck]),'--end',str(chunk[1, ck]),output1]
            subprocess.call(job_args)

    print('registration')
    if chunk.shape[1]>10:
        n = 10
    else:
        n = chunk.shape[1]
    job_args = ['mpiexec','-n',str(n),'python3', os.path.join(code_dir, 'rereg3.py'), output1, output]
    print(' '.join(job_args))
    subprocess.call(job_args)

    print('finish reg start text')

    video_names = [fn for fn in os.listdir(output)
                   if 'overview' in fn and '.mp4' in fn]
    video_names = [fn for fn in video_names
                  if not 'skip' in fn and not '-2.' in fn]
    video_names.sort()
    for k in tqdm(range(len(video_names))):
        f_v = video_names[k]
        input_f_v = output + f_v
        out_names = input_f_v[:-4] + '_vecRep'
        if not os.path.isfile(out_names):
            job_args = ['python3', os.path.join(code_dir, 'remap.py'), input_f_v, out_names]
            # print(' '.join(job_args))
            subprocess.call(job_args)


    #################################plot

    vecRep_list = []
    area_list = []
    for k in tqdm(range(len(video_names))):
        f_v = video_names[k]
        out_names = output + f_v[:-4] + '_vecRep'
        print(out_names)
        if os.path.isfile(out_names):
            vecRep, area = joblib.load(out_names)
            area = np.array(area)
            area_list.append(area)
            vecRep_list.append(vecRep)

    vecRep_all = np.vstack(vecRep_list)
    area_all = np.concatenate(area_list)
    pca2 = PCA(n_components=5, svd_solver='full')
    texPCA = pca2.fit(vecRep_all[(area_all > area_all.mean() * 0.6), :])
    points_all = texPCA.transform(vecRep_all[(area_all > area_all.mean() * 0.6), :])
    axlim = np.vstack((points_all.min(axis=0)[:3], points_all.max(axis=0)[:3]))
    for i in range(len(vecRep_list)):
        area = area_list[i]
        vecRep = vecRep_list[i][area > area_all.mean() * 0.6, :]
        text_points = texPCA.transform(vecRep)
        fs.plot_3D(text_points,axlim,azim,elev)
        plt.savefig(output + video_names[i][:-4] + '-PCA.png', dpi=300, transparent=True)
        plt.close('all')






