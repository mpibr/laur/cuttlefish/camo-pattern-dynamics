import os
import subprocess
import joblib
from tqdm import tqdm
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import umap
from sklearn.cluster import KMeans
import cv2
from texture import featurespace as fs
import numpy as np
from texture import featurespace as fs
from importlib import reload
reload(fs)
import imageio


home = os.environ['HOME']
os.chdir(home+'/Dropbox/PycharmProjects/cep_stim/')
code_dir = home+'/Dropbox/PycharmProjects/cep_stim/'



exp='sepia217-20210105-003'

input='/gpfs/laur/sepia_tools/'+ exp + '/'
output=home+'/Dropbox/lab2/sepia/' + exp + '/'

try:
    os.mkdir(output)
except:
    pass



# skip = 250
skip = 100

file_name = [fn for fn in os.listdir(output)
             if 'skip'+str(skip) + '.mp4' in fn]
print(output+file_name[0])

filename = output+file_name[0]

(vecRep, area) = joblib.load(filename[:-4]+'_vecRep')

filtered=np.squeeze((area>(area.mean()-area.std()))*(area<(area.mean()+area.std())))
# filtered=np.squeeze(area>170000)
skip_frame = np.where(filtered==0)[0]
vecRep2=vecRep[filtered,:]

cap = cv2.VideoCapture(filename)
num_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
reg = []
for n in tqdm(range(num_frames)):
    succ, img = cap.read()
    if succ:
        reg.append(img[:,:,::-1])
reg = np.array(reg)
reg = reg[filtered]
print(reg.shape)
imageio.mimwrite(filename[:-4] + '-2.mp4', reg, fps=25)

#############outliner flipped ones

reducer = umap.UMAP(n_neighbors=15,min_dist=0.0,random_state=42).fit(vecRep2)
embedding = reducer.transform(vecRep2)
plt.figure()
ax = plt.subplot()
ax.scatter(embedding[:, 0], embedding[:, 1])


Ncl=6
kmeans = KMeans(n_clusters=Ncl, random_state=42).fit(embedding)
cluster_id1=kmeans.labels_
ax.scatter(embedding[:, 0], embedding[:, 1],c=cluster_id1)
# plt.legend()
ind = np.where(cluster_id1==3)[0]
ax.scatter(embedding[ind, 0], embedding[ind, 1],c='r')

fs.plot_atlas(embedding,reg,(embedding.max()-embedding.min())//10)


for n in tqdm(range(reg.shape[0])):
    if n in ind:
        img = reg[n,...]
        M = cv2.getRotationMatrix2D((img.shape[0] / 2, img.shape[1] / 2), 180, 1)
        img1 = cv2.warpAffine(img, M, (img.shape[0], img.shape[0]))
        reg[n,...] = img1

fs.plot_atlas(embedding,reg,(embedding.max()-embedding.min())//10)
# reg = reg[filtered]
imageio.mimwrite(filename[:-4] + '-2.mp4', reg, fps=25)

############################### correct the first frame and get reg
cap = cv2.VideoCapture(filename)
num_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
reg = []
succ, img = cap.read()
img0 = img
succ, img = cap.read()
img1 = img
# img0,_=fs.rigid_reg(img0,img1)
reg.append(img0[:,:,::-1])
reg.append(img1[:,:,::-1])
for n in tqdm(range(num_frames)):
    succ, img = cap.read()
    if succ:
        reg.append(img[:,:,::-1])
reg = np.array(reg)
try:
    reg = reg[filtered]
    imageio.mimwrite(filename[:-4] + '-2.mp4', reg, fps=25)
except:
    pass

#############re-registration
reg = fs.rereg(filename[:-4] + '-2.mp4',flip=0)
#############re-map
job_args = ['python3', os.path.join(code_dir, 'remap.py'), filename[:-4] + '-2.mp4',
            filename[:-4]+'-2_vecRep_remap2']
subprocess.call(job_args)





#############re-load
(vecRep2, area2) = joblib.load(filename[:-4]+'-2_vecRep_remap2')
cap = cv2.VideoCapture(filename[:-4] + '-2.mp4')
num_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
reg = []
for n in tqdm(range(num_frames)):
    succ, img = cap.read()
    if succ:
        reg.append(img[:, :, ::-1])
reg = np.array(reg)
vecRep2 = np.array(vecRep2)
area2=np.array(area2)

#######UMAP
reducer = umap.UMAP(n_neighbors=15,min_dist=0.5,random_state=42).fit(vecRep2)
embedding = reducer.transform(vecRep)
plt.figure()
plt.scatter(embedding[:, 0], embedding[:, 1])
plt.plot(embedding[:,0],embedding[:,1])
Ncl=3
kmeans = KMeans(n_clusters=Ncl, random_state=42).fit(embedding)
cluster_id1=kmeans.labels_
plt.scatter(embedding[:, 0], embedding[:, 1],c=cluster_id1,s=60,alpha=0.5)
plt.savefig(filename[:-4]+'_UMAP.png', transparent=True)

fs.plot_atlas(embedding,reg,(embedding.max()-embedding.min())//10)
plt.savefig(filename[:-4]+'_UMAP_atlas.png', transparent=True)


#######PCA
vecRep = vecRep2
pca2 = PCA(n_components=80, svd_solver='full')
texPCA = pca2.fit(vecRep)
embedding = texPCA.transform(vecRep)

# fs.plot_3D(embedding)
# plt.figure()
# plt.scatter(embedding[:,0],embedding[:,1])
# plt.plot(embedding[:,0],embedding[:,1],linewidth=0.2,c='k')

plt.figure()
cm = plt.cm.get_cmap('RdYlBu')
z = np.arange(0, embedding.shape[0])
ax = plt.subplot()
all=ax.scatter(embedding[:,0],embedding[:,1],c=z, cmap=cm)
ax.plot(embedding[:,0],embedding[:,1],linewidth=0.2,c='k')
ax.set_aspect('equal')
cax = plt.colorbar(all, ax=ax)

Ncl=2
kmeans = KMeans(n_clusters=Ncl, random_state=42).fit(vecRep)
cluster_id1=kmeans.labels_
ax.scatter(embedding[:, 0], embedding[:, 1],c=cluster_id1,s=60,alpha=0.5)
plt.savefig(filename[:-4]+'_PCA.png', transparent=True)

ind = np.where(cluster_id1==2)[0]
ax.scatter(embedding[ind, 0], embedding[ind, 1],c='r')


fs.plot_atlas(embedding,reg,(embedding.max()-embedding.min())//10,trace=0)
plt.savefig(filename[:-4]+'_PCA_atlas.png', transparent=True)

plt.close('all')

##############find jumps
d_texture=np.diff(embedding,axis=0)
# d_texture=np.diff(vecRep,axis=0)

d_texture=fs.NDsmooth(d_texture,5,'flat')
v_texture=np.zeros(d_texture.shape[0])
for i in range(d_texture.shape[0]):
    v_texture[i] = np.linalg.norm(d_texture[i,:])
# v_texture=fs.smooth(v_texture,3,'flat')

plt.plot(v_texture)

# cluster_id1[cluster_id1<3]=1
# cluster_id1 = (cluster_id1 == 2)
threshCrosses=np.nonzero(np.diff(cluster_id1))[0]
plt.figure()
h=plt.hist(v_texture[threshCrosses],15)

v_s=v_texture[threshCrosses]
thresh = v_s.mean()+v_s.std()
thresh=h[1][np.abs(np.diff(h[0])).argmin()+1]
# thresh=h[1][h[0][1:].argmin()+1]
# thresh=600
threshCrosses=threshCrosses[v_texture[threshCrosses]>thresh]
print('threshold',thresh,'get',str(len(threshCrosses)),'transitions')



plt.figure()
ax = plt.subplot()
ax.scatter(embedding[:, 0], embedding[:, 1],c=cluster_id1)
for i in threshCrosses:
    ax.plot(embedding[(i):(i+2),0],embedding[(i):(i+2),1])
ax.set_aspect('equal')

plt.savefig(filename[:-4]+'_pick_transitions.png', transparent=True)

# plt.plot(np.diff(cluster_id1))
# plt.plot(threshCrosses,np.diff(cluster_id1)[threshCrosses],'o')


# chunkDur = offsets - onsets
# plt.figure()
# # plt.plot(v_texture)
# plt.plot(np.diff(embedding[:,0]))
# plt.plot(embedding[:,0])
# plt.scatter(threshCrosses,np.diff(embedding[:,0])[threshCrosses],s=20,c='b')
# plt.scatter(threshCrosses+1,embedding[threshCrosses+1,0],s=20,c='r')


# df=(np.diff(embedding[:,0]))
# df=v_texture
# thresh=df.std()/3


plt.figure()
win=12
onsets=[]
offsets=[]
for i in threshCrosses:
    short=np.zeros(win*2,dtype=float)
    if i < win:
        short[-(i+win):] = embedding[:(i+win),0]
    else:
        short = embedding[(i-win):(i+win),0]
    # short=fs.smooth(short,6,'flat')
    short=fs.smooth(short,3,'flat')

    plt.plot(short)
    df2=np.diff(short)
    plt.plot(df2)

    thresh = df2.mean() #+ df2.std() / 2
    # thresh = 0

    if df2[win]>0:
        on = np.where(df2[:(win+1)] < thresh)[0][-1]+1
        off = np.where(df2[win:] < thresh)[0][0] + win

        # cross=np.nonzero(np.diff(df2>thresh))[0]
    else:
        # thresh = df2.mean() + df2.std() / 2
        on = np.where(df2[:(win+1)] > thresh)[0][-1]+1
        off = np.abs(df2[win:]-thresh).argmin() + win
        # off = np.where(df2[win:] > thresh)[0][0] + win
        # cross = np.nonzero(np.diff(df2 < -thresh))[0]
    # plt.plot(cross+1,short[cross+1],'o')
    plt.plot(on, short[on], 'o')
    plt.plot(off, short[off], 'o')
    onsets.append(i - win +  on)
    offsets.append(i - win + off)
    # onsets.append(i - win +  cross[0] + 1)
    # offsets.append(i - win + cross[-1] + 1)

plt.figure()
plt.plot(embedding[:,0])
plt.scatter(onsets,embedding[onsets,0],s=20,c='r')
plt.scatter(offsets,embedding[offsets,0],s=20,c='b')

# plt.figure()
# plt.plot(v_texture)
# plt.scatter(onsets,v_texture[onsets],s=20,c='r')
# plt.scatter(offsets,v_texture[offsets],s=20,c='b')



chunkDur =np.array(offsets) - np.array(onsets)
for i in np.where(chunkDur<0)[0]:
    offsets[i] = offsets[i] + 1
    onsets[i] = onsets[i] - 1
chunkDur =np.array(offsets) - np.array(onsets)

keep_frame=np.where(filtered==1)[0]
onsets_r = keep_frame[onsets]
offsets_r = keep_frame[offsets]
chunkDur_r =np.array(offsets_r) - np.array(onsets_r)
select=(chunkDur_r-chunkDur)<10
frame_on = (onsets_r[select])*skip
frame_off = (offsets_r[select])*skip
textchunk = np.vstack((frame_on,frame_off))

outname = home + '/Dropbox/lab2/sepia/' + exp + '/' + exp
joblib.dump(textchunk,outname + '.textchunk')

onsets=np.array(onsets)[select]
offsets=np.array(offsets)[select]

chunkDur =np.array(offsets) - np.array(onsets)


###plot exmaple
cap = cv2.VideoCapture(filename[:-4] + '.mp4')
num_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
if not num_frames == vecRep.shape[0]:
    cap = cv2.VideoCapture(filename[:-4] + '-2.mp4')
for i in range(len(onsets)):
    if chunkDur[i]>6 and chunkDur[i] > 0:
        step=(offsets[i]-onsets[i])//5
        select=np.arange(onsets[i],offsets[i]+1,step)
    else:
        select=np.arange(onsets[i],offsets[i]+1)
    plt.figure(figsize=(len(select)*2,2))
    for k in range(len(select)):
        plt.subplot(1,len(select),k+1)
        cap.set(cv2.CAP_PROP_POS_FRAMES, select[k])
        succ, img = cap.read()
        plt.imshow(img[:,:,::-1])
        plt.axis('off')
        plt.title(str(select[k]*skip))
    plt.tight_layout()
    plt.savefig(output+'transition'+str(frame_on[i])+'-'+str(frame_off[i])+'.png')
    plt.close('all')

######################## 1D
state=embedding[:,0]

for i in skip_frame:
    state=np.insert(state,i,state[i-1])

dCC = np.abs(np.diff(state))

# thresh = (np.nanmax(state)+np.nanmin(state))/2
# still=state<thresh

dCC=fs.smooth(dCC,3,'flat')
plt.figure()
h = plt.hist(dCC,100)

thresh=h[1][h[0].argmin()+1]
thresh=h[1][np.abs(np.diff(h[0])).argmin()+1]
thresh = dCC.mean()+dCC.std()
# thresh=1000
still=dCC<thresh
threshCrosses=np.nonzero(np.diff(still))
print('threshold',thresh,'get',str(len(threshCrosses[0])),'transitions')



onsetLogical=still[threshCrosses]==True
offsetLogical=still[threshCrosses]==False
tC=np.asarray(threshCrosses)

onsets=tC[0,onsetLogical]+1
offsets=tC[0,offsetLogical]+2


if len(onsets)>len(offsets):
    onsets = onsets[:len(offsets)]
else:
    offsets = offsets[-len(onsets):]


chunkDur = offsets - onsets
# plt.figure()
# plt.plot(dCC)
# plt.scatter(threshCrosses[0]+1,dCC[threshCrosses[0]]+1,s=10,c='r')
plt.figure()
plt.plot(state)
plt.plot(dCC)
plt.scatter(onsets,state[onsets],s=10,c='r')
plt.scatter(offsets,state[offsets],s=10,c='b')

frame_on = (onsets)*skip
frame_off = (offsets)*skip

chunk = np.vstack((frame_on,frame_off))

outname = home + '/Dropbox/lab2/sepia/' + exp + '/' + exp
joblib.dump(chunk,outname + '.textchunk2')


###plot exmaple
cap = cv2.VideoCapture(filename[:-4] + '.mp4')
num_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
if not num_frames == area.shape[0]:
    for i in range(len(onsets)):
        onsets[i]=onsets[i]-sum(skip_frame<=onsets[i])+1
        offsets[i] = offsets[i] - sum(skip_frame <= offsets[i]) + 1
for i in range(len(onsets)):
    if chunkDur[i]>6:
        step=(offsets[i]-onsets[i])//5
        select=np.arange(onsets[i],offsets[i]+1,step)
    else:
        select=np.arange(onsets[i],offsets[i]+1)
    plt.figure(figsize=(len(select)*2,2))
    for k in range(len(select)):
        plt.subplot(1,len(select),k+1)
        cap.set(cv2.CAP_PROP_POS_FRAMES, select[k])
        succ, img = cap.read()
        plt.imshow(img[:,:,::-1])
        plt.axis('off')
        plt.title(str(select[k]*skip))
    plt.tight_layout()
    plt.savefig(output+'transition'+str(frame_on[i])+'-'+str(frame_off[i])+'.png')
    plt.close('all')

############################combine manual selection

png_list = [fn for fn in os.listdir(output+'/select/')
             if 'transition' in fn and 'png' in fn]

# png_list = [fn for fn in os.listdir(output)
#              if 'overview' in fn and 'PCA' in fn]
png_list = [fn for fn in png_list if not 'skip' in fn]
png_list.sort()
chunk = np.zeros((2,len(png_list)),dtype=int)
for i in range(len(png_list)):
    newstr = ''.join((ch if ch in '0123456789' else ' ') for ch in png_list[i])
    chunk[:,i]=[int(s) for s in newstr.replace('-',' ').split() if s.isdigit()]

# fulllengh=np.zeros((len(area)*skip),dtype=int)

fulllengh=np.zeros((chunk.max()+100),dtype=int)

for i in range(chunk.shape[1]):
    fulllengh[chunk[0,i]:chunk[1,i]] = 1
plt.plot(fulllengh)
print(chunk.shape[1])
frame_on=np.where(np.diff(fulllengh)>0)[0]+1
frame_off=np.where(np.diff(fulllengh)<0)[0]+1
chunk = np.vstack((frame_on,frame_off))
print(chunk.shape[1])

outname = home + '/Dropbox/lab2/sepia/' + exp + '/' + exp
joblib.dump(chunk,outname + '.textchunk')

###plot exmaple
try:
    os.mkdir(output+'/example/')
except:
    pass
onsets = frame_on//skip
offsets = frame_off//skip
chunkDur = offsets-onsets
cap = cv2.VideoCapture(filename[:-4] + '.mp4')
num_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
for i in range(len(onsets)):
    if chunkDur[i]>4:
        step=(offsets[i]-onsets[i])//3
        select=np.arange(onsets[i],offsets[i]+1,step)
    else:
        select=np.arange(onsets[i],offsets[i]+1)
    plt.figure(figsize=(len(select)*2,2))
    for k in range(len(select)):
        plt.subplot(1,len(select),k+1)
        cap.set(cv2.CAP_PROP_POS_FRAMES, select[k])
        succ, img = cap.read()
        plt.imshow(img[:,:,::-1])
        plt.axis('off')
        plt.title(str(select[k]*skip))
    plt.tight_layout()
    plt.savefig(output+'/example/transition'+str(frame_on[i])+'-'+str(frame_off[i])+'.png')
    plt.close('all')


plt.figure()
ax = plt.subplot()
ax.scatter(embedding[:, 0], embedding[:, 1],c=cluster_id1)
chunk = joblib.load(outname + '.textchunk')

for start,end in chunk.T:
    start = int(start/skip - sum(skip_frame <= start/skip))
    end = int(end/skip - sum(skip_frame <= end/skip))
    print(start,'-',end)
    ax.plot(embedding[start:(end+1), 0], embedding[start:(end+1), 1])
ax.set_aspect('equal')
plt.title(str(chunk.shape[1])+'_transitions')
plt.savefig(filename[:-4]+'_pick_transitions.png', transparent=True)






