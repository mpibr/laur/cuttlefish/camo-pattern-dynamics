import os
import subprocess
import joblib
from tqdm import tqdm
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import umap
from sklearn.cluster import KMeans
import cv2
from texture import featurespace as fs
import numpy as np
from texture import featurespace as fs
from importlib import reload
reload(fs)
import imageio


home = os.environ['HOME']
os.chdir(home+'/Dropbox/PycharmProjects/cep_stim/')
code_dir = home+'/Dropbox/PycharmProjects/cep_stim/'

exp_list=['sepia207-20200219-001','sepia207-20200219-002']

for exp in exp_list:
    input='/gpfs/laur/sepia_compressed/'+ exp + '/'
    output=home+'/Dropbox/lab2/sepia/' + exp + '/'
    output1 = '/gpfs/laur/sepia_processed/'+ exp + '/'
    try:
        os.mkdir(output1)
        os.mkdir(output)
    except:
        pass
    print(input)

    ###########################coarser sample
    job_args = ['python3', os.path.join(code_dir, 'texture_detectron_movie2.py'), input,
                '--start', '0','--sampling_rate','250',output1,output]
    subprocess.call(job_args)

