#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
for correlation of camouflage skin pattern and 30 naturalistic image backgrounds
example for one experiment session
fig 2a includes 13, 11, 16 such seesions for 3 animals
@author: liangx
"""

import cv2
from tqdm import tqdm
import numpy as np
import os
import matplotlib.pyplot as plt
import joblib
home = os.environ['HOME']
import sys
from texture import featurespace as fs
from importlib import reload
from sklearn.decomposition import PCA
from scipy import stats

def onclick(event):
    import matplotlib.patches as patches
    global rep
    w = mask1.shape[0]
    l = mask1.shape[1]
    # if event.dblclick:
    if event.button:
        print('%s click: button=%d, x=%d, y=%d, xdata=%f, ydata=%f' %
              ('double' if event.dblclick else 'single', event.button,
               event.x, event.y, event.xdata, event.ydata))
        rect = patches.Rectangle((event.xdata - w / 2, event.ydata - l / 2), w, l, linewidth=1, edgecolor='r',
                                 facecolor='none')
        ax.add_patch(rect)
        plt.show()
        y1 = round(event.xdata - w / 2)
        y2 = round(event.xdata + w / 2)
        x1 = round(event.ydata - l / 2)
        x2 = round(event.ydata + l / 2)
        img1 = img[x1:x2, y1:y2, :]
        cv2.imwrite(output2 + 'bg' + str(frame) + '-'+str(rep)+'.tif', img1)
        rep = rep + 1

def on_close(event):
    print('Closed Figure!')
    # next = 1

## load video
input_dir = os.getcwd()
output2 = os.getcwd() + '/bg2/'
if not os.path.isdir(output2):
    os.mkdir(output2)
file_name_list = [fn for fn in os.listdir(input_dir)
             if 'rec2_cam0' in fn and '-100X.mp4' in fn]

file_name = file_name_list[0] # select for one experiment
cap = cv2.VideoCapture(input_dir + file_name)
num_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

##find the timing of background switch
CC = joblib.load(output + "CC")
plt.plot(CC)
dCC = fs.prePCA(np.abs(np.diff(CC)))

plt.plot(dCC)
thresh = 1
still=dCC>thresh
plt.plot(still)
threshCrosses=np.nonzero(np.diff(still))
onsetLogical=still[threshCrosses]==True
offsetLogical=still[threshCrosses]==False
tC=np.asarray(threshCrosses)
onsets=tC[0,onsetLogical]
offsets=tC[0,offsetLogical]
if len(onsets)>len(offsets):
    onsets = onsets[:len(offsets)]
else:
    offsets = offsets[-len(onsets):]

chunkDur_thr = 120/4
chunkDur = offsets - onsets
onsets = onsets[chunkDur>chunkDur_thr]
offsets = offsets[chunkDur>chunkDur_thr]

plt.scatter(onsets,CC[onsets])
plt.scatter(offsets,CC[offsets])
print(len(onsets))
joblib.dump((onsets,offsets),input_dir+'chunk')


## manually selet background patches surrounding the animal
mask = cv2.imread( input_dir +'img0.tif')
mask0 = mask[:,:,0]>0
mask1 = fs.crop_frame(mask)
for frame in onsets:
    next = 0
    rep = 0
    cap.set(cv2.CAP_PROP_POS_FRAMES, frame+3)
    succ, img = cap.read()
    fig, ax = plt.subplots()
    ax.imshow(img[:,:,::-1])
    fig.canvas.mpl_connect('button_press_event', onclick)
    fig.canvas.mpl_connect('close_event', on_close)
    interval = 200  # units of seconds
    plt.pause(interval)
    plt.show()
    if rep > 5:
        plt.close()
        continue

##get texture representation from background patches
import skimage.transform
from tensorflow.keras.applications import vgg19
model = vgg19.VGG19(weights='imagenet',include_top=False)
model.compile(loss="categorical_crossentropy", optimizer="adam")

(onsets, offsets) = joblib.load(input_dir + 'chunk')
wz = 1400
mask = cv2.imread(input_dir + 'img0.tif')
mask0 = mask[:wz, :wz, 0] > 0
file_names = [fn for fn in os.listdir(output2)
              if '.tif' in fn]
vecRep_bg_list = []
for frame in tqdm(onsets):
    vecRep_bg = []
    fn_list = [x for x in os.listdir(output2)
               if 'bg' + str(frame) in x]
    if len(fn) > 0:
        for fn in fn_list:
            img_bg1 = cv2.imread(output2 + fn)
            img1 = skimage.transform.rotate(img_bg1, 90, resize=True)
            img_bg1 = (img1 * 255).astype(np.uint8)
            img_bg1 = fs.center_reg(img_bg1, wz)
            img_bg1 = fs.histN(img_bg1)
            img_bg1 = fs.apply_mask(img_bg1, mask0, no_mask=0, resize=1)
            vecRep_bg.append(fs.to_tex(img_bg1, model, 224))
    vecRep_bg_list.append(vecRep_bg)
joblib.dump(vecRep_bg_list, output2 + 'vecRep_BG_list')

##chunked skin pattern texture representation
##this part use the same script for fig 3 and was run on the HPC. please check "find_transition_step1/2/3.py"


##laod chunked skin pattern texture representation
vecRep_all = []
vecRep_bg_all = []
for exp in exp_list:
    input_dir = os.getcwd()
    file_name_fish = [fn for fn in os.listdir(input_dir)
                      if 'overview' in fn and '.mp4' in fn]
    (vecRep, area) = joblib.load(input_dir + file_name_fish[0][:-4] + '_vecRep')
    (onsets, offsets) = joblib.load(input_dir + 'chunk')
    select = np.squeeze((area > area.mean() * 0.7) * (area < area.mean() * 1.3))
    vecRep_BG_list = joblib.load(input_dir + '/bg2/' + 'vecRep_BG_list')
    vecRep_BG_list = [x for x in vecRep_BG_list if len(x)>0]
    vecRep_BG_list2 = np.vstack(vecRep_BG_list)
    vecRep_bg_all.append(vecRep_BG_list2)
    for i in range(len(onsets)):
        out_names = input_dir + '/chunks/' + file_name_fish[0][:-4] \
                    + '-' + str(onsets[i]) + '-' + str(offsets[i])
        select_chunk = select[onsets[i]:offsets[i]]
        if os.path.isfile(out_names) and select_chunk.sum() > 20:
            vecRep_new = np.array(joblib.load(out_names))
            vecRep_all.append(vecRep_new)
            if (vecRep_new.sum(axis=1)==0).sum() > 0:
                print(out_names)
                print( (vecRep_new.sum(axis=1)==0).sum() )

vecRep_all2 = np.vstack(vecRep_all)
pca2 = PCA(n_components=80, svd_solver='full')
texPCA1 = pca2.fit(vecRep_all2)
points_all = texPCA1.transform(vecRep_all2)
plt.scatter(points_all[:,0],points_all[:,1],s=5,alpha=0.1)

filter1 = points_all[:,1]<6000
pca3 = PCA(n_components=80, svd_solver='full')
texPCA2 = pca3.fit(vecRep_all2[filter1,:])
points_all = texPCA2.transform(vecRep_all2[filter1,:])
plt.scatter(points_all[:,0],points_all[:,1],s=5,alpha=0.1)

vecRep_bg_all2 = np.vstack(vecRep_bg_all)
points_bg_all = texPCA2.transform(vecRep_bg_all2)


plt.figure()
plt.scatter(points_all[:,0],points_all[:,1],s=5,alpha=0.1)
plt.scatter(points_bg_all[:,0],points_bg_all[:,1],s=50,alpha=0.5)

#### find final pattern for each trial
from sklearn.metrics import pairwise_distances
import cv2
plt.close('all')
skip = 0
swin = 5
dur_all = []
final_pattern_all = []
initial_pattern_all = []
vecRep_bg2_all = []
img_bg = []
img_final = []
vecRep_bg2_all_p = []
for exp in exp_list:
    input_dir = os.getcwd()
    file_name_fish = [fn for fn in os.listdir(input_dir)
                      if 'overview' in fn and '.mp4' in fn]
    (onsets, offsets) = joblib.load(input_dir + 'chunk')
    vecRep_bg_list = joblib.load(input_dir + '/bg2/' + 'vecRep_BG_list')
    color = plt.cm.hsv(np.linspace(0, 1, len(onsets)))
    for i in range(len(onsets)):
        vecRep_bg = vecRep_bg_list[i]
        if len(vecRep_bg)>0:
            points_bg = texPCA2.transform(vecRep_bg)
            points_bg_mean = points_bg.mean(axis=0)
            distant1 = np.squeeze(pairwise_distances(np.expand_dims(points_bg_mean, 0), points_bg))
            rep = distant1.argmin()

            out_names = input_dir + '/chunks/' + file_name_fish[0][:-4] \
                        + '-' + str(onsets[i]) + '-' + str(offsets[i])
            select_chunk = select[onsets[i]:offsets[i]]
            if os.path.isfile(out_names) and select_chunk.sum() > 20:
                vecRep_new = np.array(joblib.load(out_names))
                points_chunk_unf = texPCA1.transform(vecRep_new)
                filter2 = points_chunk_unf[:, 1] < 6000
                points_chunk = texPCA2.transform(vecRep_new[filter2,:])
                points_chunk = points_chunk[skip:,]
                if points_chunk.shape[0]>20:
                    distant = np.squeeze(pairwise_distances(np.expand_dims(points_bg_mean,0), points_chunk))
                    if distant.max() > 20:
                        distant2 = fs.prePCA(fs.smooth(distant, swin), 1)
                        dur1 = np.where(distant2<0.01)[0][0]
                        dur_all.append((dur1 + skip ) * 4)
                        if points_chunk.shape[0] - 1 < round(dur1) or dur1 < 0:
                            dur2 = distant2.argmax()
                        else:
                            dur2 = round(dur1)
                        if os.path.isfile(input_dir + '/bg2/bg'+str(onsets[i])+ '-' + str(rep) + '.tif'):
                            final_pattern_all.append(vecRep_new[filter2,:][dur2+skip])
                            initial_pattern_all.append(vecRep_new[filter2, :][0+skip])
                            vecRep_bg2_all.append(vecRep_bg[rep])
                            vecRep_bg2_all_p.append(points_bg_mean)
                            travel = np.vstack((points_chunk[0, :],points_chunk[dur2,:]))
                            frame = np.where(filter2)[0][dur2+skip]
                            cap3 = cv2.VideoCapture(out_names + '.mp4')
                            cap3.set(cv2.CAP_PROP_POS_FRAMES, frame)
                            succ, img = cap3.read()
                            img_final.append(img)
                            img2 = cv2.imread(input_dir + '/bg2/bg'+str(onsets[i])+ '-' + str(rep) + '.tif')
                            img_bg.append(img2)
                            plt.plot(travel[:, 0], travel[:, 1], color=color[i],alpha=0.5)
                            plt.scatter(points_bg[:,0], points_bg[:,1], color=color[i],s=1000,alpha=0.1)
joblib.dump((vecRep_bg2_all_p,vecRep_bg2_all,final_pattern_all,initial_pattern_all),
            input_dir+'sepia-bglist-skin_rep')

##CCA find the correlated dimension
from sklearn.cross_decomposition import CCA
cca = CCA(n_components=5)
cca.fit(final_pattern_all_p,vecRep_bg2_all_p)
final_pattern_all_p = cca.predict(final_pattern_all_p)
dim=0
plt.figure(figsize=(4,4))
ax= plt.subplot()
plt.scatter(vecRep_bg2_all_p[:, dim], final_pattern_all_p[:, dim],color='k',alpha=0.5)
coef2=stats.pearsonr(vecRep_bg2_all_p[:, dim],final_pattern_all_p[:, dim])
plt.xlabel('background texture PC1')
plt.ylabel('skin texture PC1')

##PC representation background
atlas_bg,_ = fs.plt_atlas_1d(vecRep_bg2_all_p[:,dim],img_bg,10)
plt.figure()
plt.imshow(atlas_bg)
plt.axis('off')
##PC representation animal
reload(fs)
atlas_final,example_id = fs.plt_atlas_1d(final_pattern_all_p[:,dim],img_final,10)
plt.figure()
plt.imshow(atlas_final)
plt.axis('off')
