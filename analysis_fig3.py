#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Fig 3 Dynamics of camouflage transitions
Fig 3 a-b same examples
Fig c-e summary plots using a small subset of data (the examples for Fig 3a-b)

@author: liangx
"""


##chunked skin pattern texture representation
# find_transition_step1.py to subsample (1/250) zoom-out video for texture representation
# find_transition_step2.py to identify (semi-manually) the time window of fast pattern changes
# find_transition_step3.py to read texture representation within time window of fast pattern changes at 25Hz; 
#   this part was run on the HPC. 

## load texture represenation for skin pattern and background pattern
import os
import matplotlib.pyplot as plt
from tqdm import tqdm
import numpy as np
from texture import featurespace as fs
from importlib import reload
from scipy import stats
from sklearn.decomposition import PCA
import joblib
from sklearn.metrics import pairwise_distances

home = os.environ['HOME']
os.chdir(home+'/Dropbox/analysis_liangx/')
input_dir = os.getcwd() + '/data_example/'

filenames = [fn for fn in os.listdir(input_dir)
               if 'overview' in fn ]
filenames.sort()
vecRep_list = []
area_list = []
for k in tqdm(range(len(filenames))):
    out_names = input_dir+filenames[k]
    if os.path.isfile(out_names):
        vecRep, area = joblib.load(out_names)
        area = np.array(area)
        area_list.append(np.squeeze(area))
        vecRep_list.append(vecRep)


# vecRep_all = np.vstack(vecRep_list)
# area_all = np.concatenate(area_list)
# pca2 = PCA(n_components=80, svd_solver='full')
# select = np.squeeze(area_all > area_all.mean() * 0.7) #filter out incompletely detected mantle
# texPCA = pca2.fit(vecRep_all[select, :])
# points_all = texPCA.transform(vecRep_all[select, :])
texPCA = joblib.load(input_dir+'texPCA') #load PCA model fitted with all data from selected animal

(vecRep_bg) = joblib.load(input_dir+'/vecRep_BG_avg') #background texture representation was measured by the same way as in Fig2a
points_bg = texPCA.transform(vecRep_bg)

## fig 3a

import seaborn as sns
fig = plt.figure(figsize=(4,4))
ax = plt.subplot()
ax.scatter(points_bg[0,0], points_bg[0,1],
            s=500, marker='*', c='r', alpha=.75,zorder=0)
ax.scatter(points_bg[1,0], points_bg[1,1],
            s=500, marker='*', c='g', alpha=.75,zorder=0)
ax.scatter(points_bg[2,0], points_bg[2,1],
            s=500, marker='*', c='orange', alpha=.75,zorder=0)
colors = sns.color_palette("husl", len(vecRep_list))
for i in range(len(vecRep_list)):
    text_points = texPCA.transform(vecRep_list[i])
    text_points2 = text_points.copy()
    text_smooth = fs.NDsmooth(text_points2, 25, 'flat')
    ax.plot(text_smooth[:, 0], text_smooth[:, 1], color=colors[i],alpha=.9, label=i,zorder=5)
    ax.scatter(text_smooth[0, 0], text_smooth[0, 1], color='k', s=10, alpha=1,zorder=10)
    ax.scatter(text_smooth[-1, 0], text_smooth[-1, 1], color='k', s=10, alpha=1,zorder=10)
    ax.text(text_smooth[0, 0], text_smooth[0, 1],s = 's', fontsize = 18, color='k', zorder=10)
    ax.text(text_smooth[-1, 0], text_smooth[-1, 1],s = 'e', fontsize = 18, color='k',zorder=10)
xlim = ax.get_xlim()
ylim = ax.get_ylim()
ax.set_xlabel('PC1',fontsize = 18)
ax.set_ylabel('PC2',fontsize = 18)
ax.set_xticklabels([])
ax.set_yticklabels([])
fig.tight_layout()

## fig 3b
fig = plt.figure(figsize=(4.5,4))
ax = plt.subplot()
i = 0
text_points = texPCA.transform(vecRep_list[i])
text_points2 = text_points.copy()
text_smooth = fs.NDsmooth(text_points2, 51, 'flat')
speed = fs.velocity(fs.d_F(text_smooth,1))
cm1 = plt.cm.get_cmap('hot')
ax.plot(text_smooth[:, 0], text_smooth[:, 1], color='k',linewidth=5,alpha=.5,zorder=1)
s1 = ax.scatter(text_smooth[:-1, 0], text_smooth[:-1, 1],c = speed,s = 5,
           vmin = np.quantile(speed,0.1), vmax = np.quantile(speed,.95),
           cmap = cm1, alpha = .75,zorder=5)
ax.scatter(text_smooth[0, 0], text_smooth[0, 1], color='k', s=10, alpha=1,zorder=10)
ax.scatter(text_smooth[-1, 0], text_smooth[-1, 1], color='k', s=10, alpha=1,zorder=10)
ax.text(text_smooth[0, 0], text_smooth[0, 1],s = 's', fontsize = 18, color='k', zorder=10)
ax.text(text_smooth[-1, 0], text_smooth[-1, 1],s = 'e', fontsize = 18, color='k',zorder=10)
ax.scatter(points_bg[0,0], points_bg[0,1],
            s=500, marker='*', c='r', alpha=.75,zorder=0)
ax.scatter(points_bg[1,0], points_bg[1,1],
            s=500, marker='*', c='g', alpha=.75,zorder=0)
ax.set_xlabel('PC1',fontsize = 18)
ax.set_ylabel('PC2',fontsize = 18)

cbar = fig.colorbar(s1, ax=ax, shrink=0.3, pad=0.02)
color_label = 'speed (a.u.)'
cbar.ax.set_ylabel(color_label, fontsize=20)
cbar.ax.tick_params(labelsize=18)
fig.tight_layout()

##for fig 3b & e
bg_types = ['rock', 'gray', 'sand']
k = 0 #background type = 'rock'
velocity = []
direction1 = []  # to bg
direction2 = []  # from last timepoint
direction3 = []  # start to target
sim_increase_list = [] #similarity increase for fig 3e

trough_text = []
distant_d_list = []
ndim = 2
for i in range(len(vecRep_list)):
    text_points = texPCA.transform(vecRep_list[i])
    distant1 = pairwise_distances(points_bg[k, :ndim].reshape((1, ndim)), text_points[:, :ndim])
    distant1 = fs.smooth(np.squeeze(distant1), 55)
    farpoint = distant1.argmax()
    s2bg = text_points[-1, :ndim] - text_points[0, :ndim]
    text_points2 = text_points.copy()
    text_smooth = fs.NDsmooth(text_points2, 51, 'flat')
    speed = fs.prePCA(fs.velocity(fs.d_F(text_smooth, 1), 55))
    trough = fs.find_trough(speed, 75, 0, 0)
    trough = np.unique(np.hstack((0, trough)))
    trough = np.unique(np.hstack((trough, len(speed))))
    text_points_trough = text_points2[trough, :]
    trough_text.append(text_points_trough)
    distant_d_list.append(np.diff(distant1[trough]))
    angle2_list = []
    siminlar_0 = np.corrcoef(text_points_trough[0, :50],points_bg[k, :50])[0,1]
    increase_list = []
    for j in range(len(text_points_trough)-1):
        siminlar_j =  np.corrcoef(text_points_trough[j+1, :50],points_bg[k, :50])[0,1]
        sim_increase = siminlar_j - siminlar_0
        increase_list.append(sim_increase)
        step_v = text_points_trough[j + 1, :ndim] - text_points_trough[j, :ndim]
        v2bg = text_points[-1, :ndim] - text_points_trough[j, :ndim]
        angle = (np.arctan2(np.cross(step_v, v2bg), np.dot(step_v, v2bg)))
        direction1.append(angle)
        direction2.append(np.arctan2(step_v[1], step_v[0]))
        angle2 = (np.arctan2(np.cross(step_v, s2bg), np.dot(step_v, s2bg)))
        angle2_list.append(angle2)
        velocity.append(np.linalg.norm(step_v))
    plt.close()
    direction3.append(angle2_list)
    velocity.append(np.linalg.norm(step_v))
    sim_increase_list.append(increase_list)

## fig 3c updated angle to target vs. angle to memory angle
from astropy.stats import rayleightest
from astropy import units as u

direction3 = np.hstack(direction3)
plt.figure(figsize=(4, 4))
ax2 = plt.subplot(polar=True)
n, bins, patches = fs.circular_hist(ax2, np.array(direction3), 24,
                                    density=False,color=colors[k])
plt.figure(figsize=(4, 4))
ax1 = plt.subplot(polar=True)
n, bins, patches = fs.circular_hist(ax1, np.array(direction1), 24,
                                    density=False,color=colors[k])
print('start to bg', rayleightest(direction3 * u.rad))
print('to bg', rayleightest(direction1 * u.rad))

## for fig 3d, dwell time at slow points and the number of steps
distance = []
stay = []
ndim = 2
for i in range(len(vecRep_list)):
    text_points = texPCA.transform(vecRep_list[i])
    distant1 = pairwise_distances(text_points[-1, :ndim].reshape((1, ndim)), text_points[:, :ndim])
    distant1 = fs.smooth(np.squeeze(distant1), 55)
    text_points2 = text_points.copy()
    speed1 = fs.d_F(text_points2, 55)
    speed2 = fs.d_F(fs.NDsmooth(text_points2, 55), 1)
    proj, orth = fs.projection(speed1, speed2)
    speed = fs.smooth(proj, 35)
    trough2, dwell = fs.get_dwelltime(speed)
    if len(trough2) > 1:
        distant3 = distant1[trough2]
        distance.append(distant3)
        stay.append(dwell)

##fig 3d

bins = 20
fig = plt.figure(figsize=(5, 4))
ax1 = plt.subplot()
hist_list = []
xmax = 4000
for distance1 in distance:
    hist_list.append(np.histogram(distance1,bins,(0, xmax))[0])
hist_list = np.array(hist_list)

x = np.linspace(0,xmax,bins)
width = np.diff(x).mean()
ax1.bar(x,hist_list.mean(axis=0),width-5,
        yerr=hist_list.std(axis=0)/hist_list.shape[0],color='k',alpha=0.5,
        edgecolor='k',error_kw=dict(ecolor='k', capsize=2))
ax1.set_xlabel('distance to background (a. u.)',fontsize=16,color='k')
ax1.set_ylabel('step # / trial',fontsize=16,color='k')
ax1.locator_params(nbins=5)
fig.tight_layout()
plt.tick_params(labelsize=12,colors='k')

ax2 = ax1.twinx()
dist_speed = np.vstack((np.hstack(distance), np.hstack(stay)/25))
summary = fs.binning(dist_speed, 55, 0)
ax2.plot(summary[:, 0], summary[:, 1], linewidth=1, color='r')
sem = summary[:, 2] / np.sqrt(summary[:, 3])
ax2.fill_between(summary[:, 0], summary[:, 1] - sem, summary[:, 1] + sem,
                 color='r', alpha=0.5)
ax2.set_ylabel('dwell time (s)',color='r',fontsize=16)
ax2.tick_params(axis='y', colors='red',labelsize=12)
ax2.set_xlim(3800,100)
fig.tight_layout()
plt.locator_params(nbins=5)

## fig 3e increase of similarity between skin and background as more steps of iterations
steps = 12
average_increase_list = []
#sim_increase_list = sim_increase_list[1:3]
for increase_list in sim_increase_list:
    average_increase = np.zeros((1, steps))
    for i in range(steps-1):
        if len(increase_list)>i:
            average_increase[0,i] = increase_list[i]
        else:
            average_increase[0,i] = np.nan
    if len(increase_list) > steps-1:
        average_increase[0, steps-1] = np.max(increase_list[(steps-1):])
    else:
        average_increase[0, steps-1] = np.nan
    average_increase_list.append(average_increase)
average_increase_list = np.squeeze(np.array(average_increase_list))
averaged = np.nanmean(average_increase_list,axis=0)
SEM = (np.nanstd(average_increase_list,axis=0)/np.sqrt(average_increase_list.shape[0]))

plt.figure(figsize=(3, 4))
width = 1
x = []
for i in range(steps-1):
    x.append(str(i+1))
x.append('>'+str(steps))

plt.bar(np.arange(steps), averaged, yerr=SEM, color='k', alpha=0.5,
        edgecolor='k', error_kw=dict(ecolor='k', capsize=2))
plt.xticks(np.arange(steps),x,fontsize=12)
plt.locator_params(axis='y',nbins=6)
plt.tick_params(axis='y',labelsize=12)
plt.xlabel('step',fontsize=18)
plt.ylabel('Δcorr. skin vs background',fontsize=18)
plt.tight_layout()

##

