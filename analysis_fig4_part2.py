#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Figure 4. Organisation and reorganisation of chromatophore groupings during pattern transitions.
Part 2: summary plot using a small subset of data
Fig 4d
Fig 4e
Fig 4i
@author: liangx
"""


##fig 4d pattern components: wasserstein distance vs activity correlation
import h5py
import os
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm
import joblib
from texture import featurespace as fs
home = os.environ['HOME']

from sklearn.linear_model import LinearRegression
from scipy.stats import wasserstein_distance

os.chdir(home+'/Dropbox/analysis_liangx/')
indir = os.getcwd() + '/data_example/'
(center, labels, mask) = joblib.load(indir + 'draw_parameter2')
prop = fs.find_the_large_mask(labels.T, mask.T)
points_all, mask_ind2 = joblib.load(indir + 'areas.PCA')
(areas,chunk_ind) = joblib.load( indir + '2020-11-23-14-53-15.areas')


resolution = 2
scale = 0.01775
s1 = 13 # use the trial in fig 4a-c as an example

labels_chr_name = indir + str(s1) + '-only-chrom_cluster_label' + str(int(resolution))
labels_chr = joblib.load(labels_chr_name)
mean_sub, std_sub, areas_sub, dur1 = fs.taj_decomp([s1], points_all, chunk_ind, areas, mask_ind2, labels_chr)
n_select = len(np.unique(labels_chr))
nnb_dist = np.zeros((n_select, n_select))
nnb_corr = np.zeros((n_select, n_select))
for i in range(n_select):
    for j in range(i, n_select):
        if not i == j:
            nnb_corr[i, j] = np.corrcoef((mean_sub[:, i], mean_sub[:, j]))[1][0]
            cc1 = center[:, mask_ind2[np.where(labels_chr == i)[0]] - 1]
            cc2 = center[:, mask_ind2[np.where(labels_chr == j)[0]] - 1]
            nnb_dist[i, j] = np.linalg.norm([wasserstein_distance(cc1[0, :], cc2[0, :]),
                                             wasserstein_distance(cc1[1, :], cc2[1, :])])
nnb_dist_f = nnb_dist.flatten()
nnb_corr_f = nnb_corr.flatten()
X = nnb_dist_f[nnb_dist_f!=0]*scale
Y = nnb_corr_f[nnb_dist_f!=0]
X = X[~np.isnan(Y)].reshape(-1, 1)
Y = Y[~np.isnan(Y)].reshape(-1, 1)
reg = LinearRegression().fit(X, Y)
X_test = np.linspace(X.min(), X.max(), 5).reshape(-1, 1)
Y_pred = reg.predict(X_test)
plt.figure(figsize=(4,4))
plt.scatter(X, Y, alpha=0.5,s=5,color='k')
plt.xlabel('wasserstein distance (mm)')
plt.ylabel('correlation coefficient')
plt.plot(X_test, Y_pred,color='b')
plt.title('r=' + str(reg.score(X, Y))[:6])
plt.tight_layout()

##fig 4e explained variance for different reconstrucitons
from sklearn.preprocessing import StandardScaler
import random

s1 = 19 # use the trial pair in fig 4f-h as an example
select_all = [13,19,22]

points_all, mask_ind2 = joblib.load(indir + 'areas.PCA')
(areas,chunk_ind) = joblib.load( indir + '2020-11-23-14-53-15.areas')

# find the most similar trial to s1 = 19
traj_sim = []
for s1 in tqdm(select_all):
    for s2 in select_all:
        points_sub1, dur1 = fs.select_taj([s1], points_all, chunk_ind)
        points_sub2, dur2 = fs.select_taj([s2], points_all, chunk_ind)
        norm_dist = pairwise_distances(points_sub2, points_sub1).mean() \
                    / (pairwise_distances(points_sub2).mean() + pairwise_distances(points_sub1).mean())
        traj_sim.append(norm_dist)
traj_sim = np.array(traj_sim).reshape((len(select_all), len(select_all)))
idx = np.where(np.array(select_all)==s1)[0][0]
nearest_s = np.array(select_all)[traj_sim[idx, :].argsort()][1]

#explain matrix: 'PAC', 'decomp.','nearest traj.','all traj.','static','shuffled'
mean_sub, std_sub, areas_sub, dur1 = fs.taj_decomp([s1], points_all, chunk_ind, areas_all, mask_ind2, labels_chr)
pca4 = PCA(n_components=200, svd_solver='full')
areas_sub_s = StandardScaler().fit_transform(areas_sub)
points_sub = pca4.fit_transform(areas_sub_s)
percentage = pca4.explained_variance_ratio_

explain_matrix = np.zeros((1, 6))

explain_matrix[0, 0] = percentage.sum() # PCA top 200PCs 
var_ori = points_sub.var(axis=0).sum()
labels_chr_name_list = [
    indir + str(s1) + '-only-chrom_cluster_label' + str(resolution),
    indir + str(nearest_s) + '-only-chrom_cluster_label' + str(resolution),
    indir + 'all-chrom_cluster_label' + str(resolution),
    indir + 'static-chrom_cluster_label' + str(resolution),
    indir + str(s1) + '-only-chrom_cluster_label' + str(resolution)]
ratio2 = []
for k in tqdm(range(len(labels_chr_name_list))):
    labels_chr = joblib.load(labels_chr_name_list[k])
    if k == 4:
        random.shuffle(labels_chr)
    leiden_chunk = np.copy(areas_sub_s)
    for cluster in np.unique(labels_chr):
        mask = labels_chr == cluster
        leiden_chunk[:, mask] = np.repeat(leiden_chunk[:, mask].mean(axis=1)[:, np.newaxis], mask.sum(), axis=1)
    points_sub2 = pca4.transform(leiden_chunk)
    var_re2 = points_sub2.var(axis=0).sum()
    print(k, var_re2 / var_ori)
    ratio2.append(var_re2 / var_ori)
explain_matrix[0, 1:] = ratio2

## plot one data point for fig 4e
plt.figure(figsize=(4, 4))
explain_matrix2 = explain_matrix * 100
x = np.arange(explain_matrix2.shape[1]-1)
plt.bar(x, explain_matrix2[0,1:])
xticks = ['decomp.','nearest traj.','all traj.','static','shuffled']
plt.xticks(x,xticks,rotation_mode="anchor",ha="right",
           rotation=45,fontsize=16)
plt.ylabel('Explained variance (%)',fontsize=18)
plt.locator_params(axis='y',nbins=5)
plt.tick_params(axis='y',labelsize=12)
plt.tight_layout()

##fig 4i pairwise distance between any two trials
#           vs. jaccard index of pattern decomposition by leiden clustering

from sklearn.metrics import pairwise_distances
select = [13,19,22]

j_score = []
traj_sim = []
for s1 in tqdm(select):
    for s2 in select:
        points_sub1, dur1 = fs.select_taj([s1], points_all, chunk_ind)
        points_sub2, dur2 = fs.select_taj([s2], points_all, chunk_ind)
        norm_dist = pairwise_distances(points_sub2,
                                       points_sub1).mean()
        traj_sim.append(norm_dist)
        labels_chr_name = indir + str(s1) + '-only-chrom_cluster_label' + str(resolution)
        labels_chr1 = joblib.load(labels_chr_name)
        n1 = len(np.unique(labels_chr1))
        labels_chr_name = indir + str(s2) + '-only-chrom_cluster_label' + str(resolution)
        labels_chr2 = joblib.load(labels_chr_name)
        n2 = len(np.unique(labels_chr2))
        j_mat = np.zeros((n1, n2))
        for i in range(n1):
            for j in range(n2):
                chrom_ind1 = np.where(labels_chr1 == i)[0]
                chrom_ind2 = np.where(labels_chr2 == j)[0]
                share_n = len(set(chrom_ind1).intersection(chrom_ind2))
                ratio = share_n / (len(chrom_ind1) + len(chrom_ind2) - share_n)
                j_mat[i, j] = ratio
        j_score.append(j_mat.max(axis=1).mean())
j_score = np.array(j_score).reshape((len(select),len(select)))
traj_sim = np.array(traj_sim).reshape((len(select),len(select)))

# jaccard index of shuffled clustering
import random
j_score_shuffled = []
for s1 in tqdm(select):
    labels_chr_name = indir + str(s1) + '-only-chrom_cluster_label' + str(resolution)
    labels_chr1 = joblib.load(labels_chr_name)
    labels_chr2 = labels_chr1.copy()
    for k in range(10):
        random.shuffle(labels_chr2)
        n1 = n2 = len(np.unique(labels_chr2))
        j_mat = np.zeros((n1, n2))
        for i in range(n1):
            for j in range(n2):
                chrom_ind1 = np.where(labels_chr1 == i)[0]
                chrom_ind2 = np.where(labels_chr2 == j)[0]
                share_n = len(set(chrom_ind1).intersection(chrom_ind2))
                ratio = share_n / (len(chrom_ind1) + len(chrom_ind2) - share_n)
                # ratio = share_n/(len(labels_chr1))
                j_mat[i, j] = ratio
        j_score_shuffled.append(j_mat.max(axis=1).mean())

# clean for self-comparison
for x in range(len(j_score)):
    j_score[x, x] = np.nan
j_score = j_score.flatten()
j_score = j_score[~(np.isnan(j_score))]
for x in range(len(traj_sim)):
    traj_sim[x, x] = np.nan
traj_sim = traj_sim.T.flatten()
traj_sim = traj_sim[~(np.isnan(traj_sim))]
traj_sim=traj_sim/np.linalg.norm(points_all.std(axis=0))

##plot a small subset of data for fig 4i
Y = j_score.reshape(-1, 1)
X = traj_sim.reshape(-1, 1)
reg = LinearRegression().fit(X, Y)
X_test = np.linspace(X.min(), X.max(), 5).reshape(-1, 1)
Y_pred = reg.predict(X_test)

plt.figure(figsize=(4, 4))
plt.scatter(X, Y, alpha=1, s=3, color='k')
plt.xlabel('pairwise distance (200 PCs)')
plt.ylabel('Mean IoU')
x = [X.min(), X.max()]
y = [np.mean(j_score_shuffled), np.mean(j_score_shuffled)]
plt.plot(x, y)
plt.fill_between(x, y - np.std(j_score_shuffled),
                 y + np.std(j_score_shuffled),alpha=0.3)
plt.tight_layout()

##

