# camo-pattern-dynamics

This repository contains the analysis code for the following article.

> Woo, T.\*, Liang, X.\*, Evans, D.A. et al. The dynamics of pattern matching in camouflaging cuttlefish. Nature (2023). https://doi.org/10.1038/s41586-023-06259-2
