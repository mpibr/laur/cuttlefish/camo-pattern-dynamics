#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
Helper functions for analysis. 
@author: woot
"""

import os
from glob import glob
import numpy as np
import cv2
import h5py

def get_session_type(vid_list, outdir):
    """Get session type (ex1234) based on file dir labelling."""
    import os
    from glob import glob

    session_list = np.array([os.path.split(os.path.dirname(f))[1] for f in vid_list])

    type_list = []
    for sess in session_list:
        ex_type = os.path.split(os.path.dirname(glob('{}/ex*/{}*'.format(
            outdir, sess))[0]))[-1]
        type_list.append(ex_type)
    type_list = np.array(type_list)
    
    return session_list, type_list

def load_activations(activation_files):
    activations = []
    vid_idx = np.empty(0, dtype='int')
    frame_numbers = np.empty(0, dtype='int')
    areas = np.empty(0, dtype='float32')
    positions = np.empty((0, 2), dtype='float32')

    vid_dict = []
    radius_dict = []
    
    for i, actfile in enumerate(activation_files):
        with h5py.File(actfile, 'r') as hf:
            r = int(hf.attrs['input_length'] / 2)
            radius_dict.append(r)
            videofile = os.path.abspath(
                os.path.join(os.path.dirname(hf.filename), hf.attrs['video']))
            vid_dict.append(videofile)
            frames = hf['frame_numbers'][:]
            ar = hf['areas'][:]/r**2
            
            activations.append(hf['activations/original'][:])
            vid_idx = np.append(vid_idx, np.repeat([i], len(frames)))
            frame_numbers = np.append(frame_numbers, frames)
            areas = np.append(areas, ar)
            positions = np.append(positions, hf['positions'][:], axis=0)

    return np.concatenate(activations), vid_idx, frame_numbers, areas, positions, \
        np.array(vid_dict), np.array(radius_dict)
        
def remove_lines(fig=None, ticks=True, spines=True, axis=None):
    import matplotlib.pyplot as plt

    if axis is not None:
        axis_list = [axis]
    else:
        if fig is None:
            fig = plt.gcf()
        
        axis_list = fig.get_axes()
        
    for ax in axis_list:
        if ticks:
            ax.set_xticks([])
            ax.set_yticks([])
            ax.minorticks_off()
            try:
                ax.set_zticks([])
            except:
                pass
        
        if spines:
            for spine in ax.spines.values():
                spine.set_visible(0)

def remove_padding(fig=None, show_title=False,show_suptitle=False):
    import matplotlib.pyplot as plt
    if fig is None:
        fig = plt.gcf()
    if show_title:
        fig.subplots_adjust(left=0, bottom=0.06, right=1, top=0.94, wspace=None, hspace=None) 
    elif show_suptitle:
        voffset = 100/(fig.get_size_inches()[0]*fig.dpi)
        fig.subplots_adjust(left=0.05, bottom=voffset, right=0.99, top=1-voffset, wspace=0.2, hspace=None)
        fig.set_size_inches(fig.get_size_inches()[0], fig.get_size_inches()[1]+voffset*2)     
    else:
        fig.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=None, hspace=None) 
        

def manual_flip_pattern_map(grid, index, n_grid, origin='lower'):
    """
    index : list of (r, c) to be flipped
    n_grid : (n_rows, n_cols)
    """
    n_rows, n_cols = n_grid
    h, w = grid.shape[:2]
    grid_y = h // n_rows
    grid_x = w // n_cols
    
    grid_new = grid.copy()
    for r, c in index:
        if origin == 'lower':
            r = n_rows -1 - r
            
        im = grid[grid_y*r : grid_y*(r+1), grid_x*c : grid_x*(c+1)]
        flipped = im[::-1]
        grid_new[grid_y*r : grid_y*(r+1), grid_x*c : grid_x*(c+1)] = flipped
        
    return grid_new

def get_queenframe(areasfile):
    with h5py.File(areasfile, 'r') as hf:
        cqfile = os.path.join(os.path.dirname(hf.filename), hf.attrs['cleanqueen'])
        
    with h5py.File(cqfile, 'r') as hf:
        cq = hf['Chromatophore'][:]
        qfile = os.path.join(os.path.dirname(hf.filename), hf.attrs['queenframe'])

    with h5py.File(qfile, 'r') as hf:
        qf = hf['Chromatophore'][:]
    return qf

def get_cleanqueen(areasfile):
    with h5py.File(areasfile, 'r') as hf:
        cqfile = os.path.join(os.path.dirname(hf.filename), hf.attrs['cleanqueen'])
        
    with h5py.File(cqfile, 'r') as hf:
        cq = hf['Chromatophore'][:]
    return cq

def cuttlefish_mask(masterframe, percentile=50, holes=False):
    from skimage import morphology
    from scipy import ndimage

    gSigma1=2
    gSize1=15
    gSigma2=1.5
    gSize2=15
    gSigma3=30

    if holes:
        gSize3=int((2*gSigma3)+1)
    else:
        gSize3=int(2*(2*gSigma3)+1)

    minSize=1000000
    blur1 = cv2.GaussianBlur(masterframe, (gSize1,gSize1), gSigma1)
    blur2 = cv2.GaussianBlur(masterframe, (gSize2,gSize2), gSigma2)
    blur3=blur2-blur1
    mask = np.zeros_like(blur3, dtype=bool)
    mask[blur3>0.1] = True
    blur3 = cv2.GaussianBlur(blur3*mask, (gSize3,gSize3), gSigma3)
    mask = np.zeros_like(blur3, dtype=bool)
    mask[blur3>np.percentile(blur3, percentile)] = True
    cleaned = morphology.remove_small_objects(mask, minSize, 2)
    labels, num_labels = ndimage.label(cleaned)
    if num_labels==0:
        cmask = np.ones_like(labels, dtype='bool_')
        return cmask
    else:
        cuttlefish_label = np.argmax(np.bincount(labels.flatten())[1 : ]) + 1
        return labels == cuttlefish_label

def get_mantle_mask(areasfile, percentile):
    with h5py.File(areasfile, 'r') as hf:
        cqfile = os.path.join(os.path.dirname(hf.filename), hf.attrs['cleanqueen'])

    with h5py.File(cqfile, 'r') as hf:
        cq = hf['Chromatophore'][:]
        qfile = os.path.join(os.path.dirname(hf.filename), hf.attrs['queenframe'])

    with h5py.File(qfile, 'r') as hf:
        qf = hf['Chromatophore'][:]

    mantle_mask = cuttlefish_mask(qf, percentile).astype('uint8')
    return mantle_mask

def get_rotation_matrix(qf_mask, flip=False, shape=None):
    """Usage: cv2.warpAffine(im, roMat, im.shape[:2])"""
    
    if shape is None:
        shape = qf_mask.shape

    contours, _ = cv2.findContours(qf_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contour_id = np.argsort([cv2.contourArea(x) for x in contours])[::-1][0]
    ellipse = cv2.fitEllipse(contours[contour_id])
    x, y = ellipse[0]
    ma = np.zeros(shape,dtype='uint8')
    cv2.ellipse(ma, ellipse, 1, -1)
    if flip:
        roMat = cv2.getRotationMatrix2D(ellipse[0], ellipse[2], -1.0)
    else:
        roMat = cv2.getRotationMatrix2D(ellipse[0], ellipse[2], 1.0)
    roMat[0,2] -= (x - shape[0]/2)
    roMat[1,2] -= (y - shape[1]/2)
    
    return roMat

def filter_areas(areasfile, cq, mask, trim_end=50, excluded_chunks=[]):
    """
    return : areas, chunk_indexes, original_frames, included_ch, ch_inverse
    """
    
    included_ch, ch_inverse = np.unique(cq[mask==1], return_inverse=True)
    included_ch -= 1
    
    areas, chunk_indexes, original_frames = load_areas(areasfile, included_ch)
    
    selected_chunks = np.unique(chunk_indexes)[~np.in1d(np.unique(chunk_indexes), excluded_chunks)]
    
    selected_idx = []
    all_idx = np.arange(len(areas), dtype='int32')
    for chunk_id in selected_chunks:
        selected_idx.append(all_idx[chunk_indexes==chunk_id][:-trim_end])
    selected_idx = np.concatenate(selected_idx)
    
    areas = areas[selected_idx]
    chunk_indexes = chunk_indexes[selected_idx]
    original_frames = original_frames[selected_idx]
    
    return areas, chunk_indexes, original_frames, included_ch, ch_inverse