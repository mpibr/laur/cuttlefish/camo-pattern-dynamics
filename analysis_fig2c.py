#!/usr/bin/env python
# coding: utf-8
"""
Figure 2c. The relationship between camouflage and chequerboard backgrounds in chromatophore space. 
One of three animals analysed.
@author: woot
"""

import os
import datetime

import numpy as np
import cv2
import matplotlib.pyplot as plt
from tqdm import tqdm
import h5py

import seaborn as sns
sns.set_theme(style='ticks', palette='tab10')

from sklearn.decomposition import PCA
from scipy.stats import zscore
from scipy.stats import pearsonr
from sklearn.preprocessing import StandardScaler

import scanpy
import anndata as ad
from scipy import ndimage

from matplotlib.offsetbox import AnchoredText
plt.rcParams['svg.fonttype'] = 'none'

from texture import util
import analysis_util

## Define paths and variables

OVERVIEW_VID_DIRS = ['data_example/input_dir']
OUTDIR = 'data_example/output_dir'

CUTTLEFISH_MODEL = 'data_example/models/cuttlefish-seg_20200605.pth'
CUTTLEFISH_MODEL_BASE = 'COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml'
ROTATE_MODEL = 'data_example/models/cuttlefish-kp_20210820b.pth'
ROTATE_MODEL_BASE = 'COCO-Keypoints/keypoint_rcnn_R_50_FPN_3x.yaml'
MODEL_TYPE = 'detectron2'
LAYER = 'block5_conv1'
RANDOM_SEED = 99

FONT_SIZE = 18
FIELD_OF_VIEW_CM = 0.3906*13
FIELD_OF_VIEW_PX = 3000

FIG_DIR = 'figures/'

###########################################################

## Load model

cuttlefish_model = util.load_detectron2_model(CUTTLEFISH_MODEL, CUTTLEFISH_MODEL_BASE)
upright_model = util.load_detectron2_model(ROTATE_MODEL, ROTATE_MODEL_BASE)

## Load data

STIMFILE_LIST = [os.path.join(OUTDIR, 'sepia211/sepia211.stim')]

AREASFILE_LIST = []
for sf in STIMFILE_LIST:
    with h5py.File(sf, 'r') as hf:
        rel_path = hf.attrs['areas']
        full_path = os.path.join(
            os.path.dirname(hf.filename), rel_path)
    AREASFILE_LIST.append(full_path)
    
AREASFILE = AREASFILE_LIST[0]

### Load areas

TRIM_END = 50
EXCLUDE_CHUNKS = []

qf = analysis_util.get_queenframe(AREASFILE)
cq = analysis_util.get_cleanqueen(AREASFILE)
mantle_mask_pca = analysis_util.get_mantle_mask(AREASFILE, 70)
mantle_mask_pca_filled = ndimage.binary_fill_holes(mantle_mask_pca).astype('uint8')
mantle_mask_pca_filled_mantle = ndimage.binary_fill_holes(analysis_util.get_mantle_mask(AREASFILE, 60)).astype('uint8')
print(mantle_mask_pca.sum()/mantle_mask_pca_filled.sum())
print(mantle_mask_pca.sum()/mantle_mask_pca_filled_mantle.sum())
roMat = analysis_util.get_rotation_matrix(mantle_mask_pca)
rotated_mask = cv2.warpAffine(mantle_mask_pca, roMat, mantle_mask_pca.shape[:2])
areas, chunk_indexes, original_frames, included_ch, ch_inverse = analysis_util.filter_areas(\
    AREASFILE, cq, mantle_mask_pca, trim_end=TRIM_END, excluded_chunks=EXCLUDE_CHUNKS)
# 
scaler = StandardScaler()

print(areas.shape, len(np.unique(chunk_indexes)))

fig, ax = plt.subplots(1, 5)
ax[0].imshow(qf)
ax[1].imshow(mantle_mask_pca_filled_mantle)
ax[2].imshow(mantle_mask_pca_filled)
ax[3].imshow(mantle_mask_pca)
ax[4].imshow(rotated_mask)
plt.show()

###########################################################

## Chromatophore-level PCA

### Load areas

TRIM_END = 50
EXCLUDE_CHUNKS = []
cq = analysis_util.get_cleanqueen(AREASFILE)
mantle_mask_pca = analysis_util.get_mantle_mask(AREASFILE, 70)
roMat = analysis_util.get_rotation_matrix(mantle_mask_pca)
rotated_mask = cv2.warpAffine(mantle_mask_pca, roMat, mantle_mask_pca.shape[:2])

areas, chunk_indexes, original_frames, included_ch, ch_inverse = analysis_util.filter_areas(\
    AREASFILE, cq, mantle_mask_pca, trim_end=TRIM_END, excluded_chunks=EXCLUDE_CHUNKS)

areas_z = zscore(areas, axis=0)

print(areas.shape, len(np.unique(chunk_indexes)))

fig, ax = plt.subplots(1, 2)
ax[0].imshow(mantle_mask_pca)
ax[1].imshow(rotated_mask)
plt.show()

### PCA

DATA = areas_z
PCA_THRESHOLD = 0.8

pca_ar = PCA(n_components=200, random_state=RANDOM_SEED)
X_pca_ar = pca_ar.fit_transform(DATA)  # n_samples, n_features
cumsum = np.cumsum(pca_ar.explained_variance_ratio_)
n_components = (cumsum > PCA_THRESHOLD).sum()

X_pca_ar = X_pca_ar[:,:n_components]
print('PCA:', X_pca_ar.shape)

fig, ax = plt.subplots()
analysis_util.remove_padding()
ax.plot(cumsum, c='k')

cross_threshold = (cumsum > PCA_THRESHOLD).nonzero()[0]
if len(cross_threshold) > 0:
    n_components = cross_threshold[0]
    ax.axvline(n_components, c='b')
    
ax.set_ylabel('Variance explained')
ax.set_xlabel('PCs')
plt.show()

fig, ax = plt.subplots()
plt.bar(np.arange(50),pca_ar.explained_variance_ratio_[:50], color='k')
ax.set_ylabel('Variance explained')
ax.set_xlabel('PC')
plt.show()

### Save PCA


PCA_FILE = os.path.join(OUTDIR, 'sepia211_checkerboard.pca')
stimfile = os.path.splitext(AREASFILE)[0] + '.stim'
print(stimfile)

with h5py.File(stimfile, 'r') as hf:
    stim_order = hf['stim_order'][:]
    square_standard_cm = hf['square_standard_cm'][:]

with h5py.File(PCA_FILE, 'r') as hf:
    hf.create_dataset('X_pca_ar', data=X_pca_ar, dtype='float32')
    hf.attrs['areas'] = os.path.relpath(os.path.dirname(hf.filename), AREASFILE)
    hf.attrs['stim'] = os.path.relpath(os.path.dirname(hf.filename), stimfile)
    hf.create_dataset('components', data=pca_ar.components_, dtype='float32')
    hf.create_dataset('ch_inverse', data=ch_inverse, dtype='int32')
    hf.create_dataset('pca_stim', data=stim_order[chunk_indexes], dtype='uint8')
    hf.create_dataset('square_standard_cm', data=square_standard_cm, dtype='float32')
    hf.create_dataset('mantle_mask_pca', data=mantle_mask_pca, dtype='uint8')
    hf.create_dataset('explained_variance', data=pca_ar.explained_variance_ratio_, dtype='float32')
    hf.create_dataset('chunk_indexes', data=chunk_indexes, dtype='int32')
    hf.create_dataset('original_frames', data=original_frames, dtype='int32')

###########################################################

## Leiden clustering

STIM_MAX = 5
STIM_MIN = 0

PCA_FILE = os.path.join(OUTDIR, 'sepia211_checkerboard.pca')
with h5py.File(PCA_FILE, 'r') as hf:
    pca_stim = hf['pca_stim'][:]
    square_standard_cm = hf['square_standard_cm'][:]
    chunk_indexes = hf['chunk_indexes'][:]
    
data_filter = (square_standard_cm[pca_stim] >= STIM_MIN) & (square_standard_cm[pca_stim] <= STIM_MAX)
pca_stim = pca_stim[data_filter]
areas = areas[data_filter]
chunk_indexes = chunk_indexes[data_filter]

### Leiden community clustering using scanpy

DATA = areas
N_COMPS = 50
N_NEIGHBOURS = 10
RESOLUTION = 2

adata = ad.AnnData(DATA.T)
scanpy.tl.pca(adata, n_comps=N_COMPS, svd_solver='arpack')

scanpy.pp.neighbors(adata, n_neighbors=N_NEIGHBOURS, n_pcs=N_COMPS)
scanpy.tl.leiden(adata, resolution=RESOLUTION)

cluster_labels = adata.obs['leiden'].to_numpy().astype('int')

### Average area by Leiden cluster

DATA = areas
CLUSTERING_LABELS = cluster_labels
CHUNK_ID = chunk_indexes
STIM = pca_stim

mean_all = []

for l in np.unique(CLUSTERING_LABELS):
    d = DATA[:,CLUSTERING_LABELS==l]
    chunk_mean = np.array([d[CHUNK_ID==c].mean() for c in np.unique(CHUNK_ID)])
    
    mean_all.append(chunk_mean)
    
mean_all = np.array(mean_all)
chunk_stim = np.array([STIM[CHUNK_ID==c][0] for c in np.unique(CHUNK_ID)])
stim_x = square_standard_cm[chunk_stim]

### Save leiden

LEIDEN_FILE = os.path.join(OUTDIR, 'sepia211b.leiden')
with h5py.File(LEIDEN_FILE, 'r') as hf:
    hf.create_dataset('stim_x', data=stim_x)
    hf.create_dataset('mean_all', data=mean_all)
    hf.create_dataset('cluster_labels', data=cluster_labels)

###########################################################

## Panel c: Plot chromatophore areas vs checkerboard square size, by cluster

### Load PCA

PCA_FILE = os.path.join(OUTDIR, 'sepia211_checkerboard.pca')
with h5py.File(PCA_FILE, 'r') as hf:
    pca_stim = hf['pca_stim'][:]
    square_standard_cm = hf['square_standard_cm'][:]
    ch_inverse = hf['ch_inverse'][:]


### Load leiden

LEIDEN_FILE = os.path.join(OUTDIR, 'sepia211b.leiden')
with h5py.File(LEIDEN_FILE, 'r') as hf:
    mean_all = hf['mean_all'][:]
    stim_x = hf['stim_x'][:]
    cluster_labels = hf['cluster_labels'][:]

### Calculate Pearson's correlation per cluster

DATA = mean_all
STIM = stim_x

r_coef_list = []
p_list = []

for chunk_mean in DATA:
    r, pv = pearsonr(np.log2(STIM), chunk_mean)
    r_coef_list.append(r)
    p_list.append(pv)
    
r_coef_list = np.array(r_coef_list)
p_list = np.array(p_list)

### Make mask

cq = analysis_util.get_cleanqueen(AREASFILE)
mantle_mask_pca = analysis_util.get_mantle_mask(AREASFILE, 70)
roMat = analysis_util.get_rotation_matrix(mantle_mask_pca)
rotated_mask = cv2.warpAffine(mantle_mask_pca, roMat, mantle_mask_pca.shape[:2])

### Plot

CLUSTERING_LABELS = cluster_labels
MANTLE_MASK = mantle_mask_pca
R_COEF = r_coef_list
P_LIST = p_list
P_THRESHOLD = 0.05
CHROMA_INVERSE = ch_inverse
CLUSTER_MEAN = mean_all
STIM = stim_x
AR_STIM = square_standard_cm[pca_stim]
N_SAMPLES = 5
N_XLABELS = 4

N_COLS = 4

## Select data examples for main figure
cluster_order = []
d_subset = np.where((P_LIST < P_THRESHOLD)&(R_COEF > 0))[0]
d_subset = d_subset[np.argsort(R_COEF[d_subset])[::-1]][2:5:2]
cluster_order += list(d_subset[:N_SAMPLES])
d_subset = np.where((P_LIST < P_THRESHOLD)&(R_COEF < 0))[0]
d_subset = d_subset[np.argsort(R_COEF[d_subset])][:3:2]
cluster_order += list(d_subset[:N_SAMPLES])
cluster_order = np.array(cluster_order)

n_rows = int(np.ceil(len(np.unique(CLUSTERING_LABELS)[cluster_order])/N_COLS))

## Figure

fig, ax = plt.subplots(n_rows*2, N_COLS, figsize=(N_COLS*4, n_rows*2*4))

## Make cluster location on skin
cluster_skin = np.zeros_like(MANTLE_MASK, dtype='float32')
cluster_skin[MANTLE_MASK==1] = CLUSTERING_LABELS[CHROMA_INVERSE]+1
cluster_skin = cv2.warpAffine(cluster_skin, roMat, cluster_skin.shape[:2], borderValue=0)

### Background mantle
qf = analysis_util.get_queenframe(AREASFILE)
qf_warped = cv2.warpAffine(qf, roMat, qf.shape[:2], borderValue=0)

for i, (c, chunk_mean) in enumerate(
        zip(np.unique(CLUSTERING_LABELS)[cluster_order], CLUSTER_MEAN[cluster_order])):
    axskin = ax.flat[i]
    axreg = ax.flat[n_rows*N_COLS+i]

### Place cluster on skin
curr_skin = cluster_skin.copy()
curr_skin[cluster_skin>0] = 1
curr_skin[cluster_skin==c+1] = 2

## Define colour scheme

if P_LIST[cluster_order][i] < P_THRESHOLD:
    if R_COEF[cluster_order][i] > 0:
        colormap = 'coolwarm'
        clr = 'r'
    else:
        colormap = 'coolwarm_r'
        clr = 'b'
else:
    colormap = 'Greys'
    clr = 'grey'
    
## Cluster location on skin
axskin.imshow(np.ma.masked_values(curr_skin, 0), cmap=colormap, origin='lower', vmin=0, vmax=2)
sns.regplot(x=STIM, y=chunk_mean,
            ax=axreg, logx=True, scatter_kws={"s": 10}, color='k')

## Scatter + line fit
axreg.set_xscale('log')
axreg.set_ylim(-1.5,1.5)
at = AnchoredText(
    '$r = {:.02f}$'.format(R_COEF[cluster_order][i]),
    prop=dict(size=FONT_SIZE), frameon=False, loc='upper right',pad=0.2)
axreg.add_artist(at)

## Side histogram
axhist = ax.flat[n_rows*N_COLS+i].twiny()
sns.histplot(y=chunk_mean, ax=axhist, kde=True, color=clr)

## Formatting

axhist.set_xlim(0,100)
axhist.set_ylim(ax.flat[n_rows*N_COLS+i].get_ylim())
axhist.set_xticks([])
axhist.set_xlabel('')
axhist.minorticks_off()

if i == 0:
    axreg.set_yticks([-1,0,1])
    axreg.set_yticklabels(['{:.0f}'.format(x) for x in axreg.get_yticks()], fontsize=FONT_SIZE)
    axreg.set_ylabel('Mean area (z-score)', fontsize=FONT_SIZE)
    axreg.minorticks_off()
    
    axreg_xticks = np.logspace(np.log10(STIM.min()), np.log10(STIM.max()), N_XLABELS)
    axreg.set_xticks(axreg_xticks)
    axreg.set_xticklabels(['{:.02f}'.format(x) for x in axreg.get_xticks()], fontsize=FONT_SIZE)
    axreg.set_xlabel('Square size (cm)', fontsize=FONT_SIZE)
else:
    analysis_util.remove_lines(axis=axreg, spines=False)
    analysis_util.remove_lines(axis=axskin)
    axreg.set_xlim(axreg_xticks.min(), axreg_xticks.max())
    
plt.show()

today_str = datetime.datetime.now().strftime('%Y%m%d')
OUTPUT_FIG = os.path.join(FIG_DIR, f'fig2/c_leiden_{today_str}.svg')
fig.savefig(OUTPUT_FIG, dpi=300, bbox_inches='tight', transparent=True)
